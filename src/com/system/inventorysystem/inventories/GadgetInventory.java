package com.system.inventorysystem.inventories;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import com.system.hubsystem.manager.ItemManager;
import com.system.ranksystem.manager.DataManager;

public class GadgetInventory {
	
	public static void onMethod(Player p, String m){
		switch(m.toUpperCase()){
		case("CREATE"):
			createGadgetInventory(p);
			break;
		case("OPEN"):
			onOpen(p);
			break;
		}
	}
	private static void createGadgetInventory(Player p){
		Inventory inv = Bukkit.createInventory(p, 54, "Gadgets: " + ChatColor.ITALIC + "" +  p.getName());				
		
		inv.setItem(10, ItemManager.Mounts() );
		inv.setItem(11, ItemManager.FlowerPower());
		inv.setItem(12, ItemManager.Slapper());
		inv.setItem(13, ItemManager.Fireball());
		inv.setItem(14, ItemManager.FireWork());

		DataManager.pgadgetinv.put(p.getName(), inv);
	}
	
	private static void onOpen(Player p){
		p.openInventory(DataManager.pgadgetinv.get(p.getName()));
			
		
	}
}
