package com.system.inventorysystem.inventories;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import com.system.hubsystem.manager.ItemManager;
import com.system.ranksystem.manager.DataManager;

public class StaticsInventory {
	
	public static void onMethod(Player p, String m){
		switch(m.toUpperCase()){
		case("CREATE"):
			createInventoryStatics(p);
			break;
		case("OPEN"):
			onOpen(p);
			break;
		}
	}
	private static void createInventoryStatics(Player p){
		Inventory inv = Bukkit.createInventory(p, 54, "Statics: " + ChatColor.ITALIC + "" +  p.getName());				
				
		DataManager.pinvstatics.put(p.getName(), inv);
	}
	
	private static void onOpen(Player p){
		p.openInventory(DataManager.pinvstatics.get(p.getName()));
			
		
	}

}
