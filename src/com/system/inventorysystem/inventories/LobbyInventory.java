package com.system.inventorysystem.inventories;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.system.hubsystem.manager.ItemManager;
import com.system.ranksystem.manager.DataManager;

public class LobbyInventory {

	public static void onMethod(Player p, String m){
		switch(m.toUpperCase()){
		case("CREATE"):
			createInventoryLobby(p);
			break;
		case("OPEN"):
			onOpen(p);
			break;
		}
	}
	private static void createInventoryLobby(Player p){
		Inventory inv = Bukkit.createInventory(p, 9, "Lobbies: " + ChatColor.ITALIC + "" +  p.getName());				
		
		
		inv.setItem(1, ItemManager.LobbyItem());
		inv.setItem(2, ItemManager.LobbyItem1());
		inv.setItem(3, ItemManager.LobbyItem2());
		inv.setItem(4, ItemManager.LobbyItem3());
		inv.setItem(5, ItemManager.LobbyItem4());
		inv.setItem(6, ItemManager.LobbyItem5());
		inv.setItem(7, ItemManager.LobbyItem6());
		
		DataManager.plobbyinv.put(p.getName(), inv);
	}
	
	private static void onOpen(Player p){
		p.openInventory(DataManager.plobbyinv.get(p.getName()));

	}
}
