package com.system.inventorysystem.inventories;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import com.system.hubsystem.manager.ItemManager;
import com.system.ranksystem.manager.DataManager;

public class TeleportInventory {

	public static void onMethod(Player p, String m){
		switch(m.toUpperCase()){
		case("CREATE"):
			createInventoryTeleport(p);
			break;
		case("OPEN"):
			onOpen(p);
			break;
		}
	}
	private static void createInventoryTeleport(Player p){
		Inventory inv = Bukkit.createInventory(p, 45, "Teleporter: " + ChatColor.ITALIC + "" +  p.getName());				
		
		inv.setItem(18, ItemManager.Hub());
		inv.setItem(10, ItemManager.Games());

		DataManager.pteleporterinv.put(p.getName(), inv);
	}
	
	private static void onOpen(Player p){
		p.openInventory(DataManager.pteleporterinv.get(p.getName()));
			
		
	}
}
