package com.system.inventorysystem.inventories;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import com.system.hubsystem.manager.ItemManager;
import com.system.ranksystem.manager.DataManager;

public class ShopInventory {

	public static void onMethod(Player p, String m){
		switch(m.toUpperCase()){
		case("CREATE"):
			createShopInventory(p);
			break;
		case("OPEN"):
			onOpen(p);
			break;
		}
	}
	private static void createShopInventory(Player p){
		Inventory inv = Bukkit.createInventory(p, 54, "Shop: " + ChatColor.ITALIC + "" +  p.getName());				
		
		inv.setItem(10, ItemManager.gadgetsitem() );
		inv.setItem(12, ItemManager.Mounts() );
		inv.setItem(14, ItemManager.Pets() );
		inv.setItem(16, ItemManager.Ingame() );
		inv.setItem(29, ItemManager.Disguises() );
		inv.setItem(31, ItemManager.PartiesFriends() );
		inv.setItem(33, ItemManager.Particles() );

		DataManager.pshopinv.put(p.getName(), inv);

	}
	
	private static void onOpen(Player p){
		p.openInventory(DataManager.pshopinv.get(p.getName()));
			
		
	}
}