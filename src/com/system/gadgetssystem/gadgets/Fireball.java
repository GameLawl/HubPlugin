package com.system.gadgetssystem.gadgets;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.system.ranksystem.Main;
import com.system.ranksystem.ParticleEffect;

public class Fireball {
	public static HashMap<String, Integer> task = new HashMap<String, Integer>();

	public static void onLaunch(final Player p){
		Vector from = p.getLocation().toVector();
		Vector to = p.getTargetBlock(null, 100).getLocation().toVector();
		Vector direction = to.subtract(from);
		direction.normalize();
		direction.multiply(2); // Set speed to 2

		final Entity fire = (Entity) p.getWorld().spawnEntity(p.getLocation().add(0, 3, 0), EntityType.FIREBALL);
		
		fire.setVelocity(direction);
		
	}
}
