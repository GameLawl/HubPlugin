package com.system.bansystem;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import com.system.ranksystem.Main;
import com.system.ranksystem.manager.DataManager;
import com.system.ranksystem.manager.PermissionManager;

public class CommandManager2 implements CommandExecutor {

	public static String prefix = ChatColor.GREEN + "COMMAND>> ";
	public static String prefix1 = ChatColor.DARK_RED + "BAN>> ";

	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String tag, String[] args)
	{
		
	  if(sender instanceof Player){
		  Player p = (Player) sender;
		  if(PermissionManager.hasPermission(p, "moderator") == true){
	  if (tag.equalsIgnoreCase("ban"))
	  {
	    if (args.length < 2)
	    {
	      sender.sendMessage(prefix + ChatColor.RED + "/ban <username> <banreason>");
	      return true;
	    }
	    if (args.length > 1)
	    {
	      Player online = Bukkit.getPlayer(args[0]);
	      if (online == null)
	      {
	        OfflinePlayer offline = Bukkit.getOfflinePlayer(args[0]);
	        FileConfiguration config = null;
	        File file = new File("plugins/" + File.separator + "Bans" + File.separator + "players" + File.separator + args[0].toLowerCase() + ".yml");
	        config = YamlConfiguration.loadConfiguration(file);
	        StringBuilder x = new StringBuilder();
	        for (int i = 1; i < args.length; i++) {
	          x.append(args[i] + " ");
	        }
	        config.set("ban_reason", x.toString().trim());
	        config.set("ban_sender", sender.getName());
	        try
	        {
	          config.save(file);
	        }
	        catch (Exception e)
	        {
	          e.printStackTrace();
	          sender.sendMessage(prefix + ChatColor.RED + "Player Data could not be saved");
	        }
	        offline.setBanned(true);
	        Bukkit.broadcastMessage(prefix1 + ChatColor.GRAY + offline.getName() + " has been banned by " + DataManager.s(p) + "" + ChatColor.GRAY + " " + sender.getName() + " for " + x.toString().trim());
	      }
	      else
	      {
	        FileConfiguration config = null;
	        File file = new File("plugins/" + File.separator + "Bans" + File.separator + "players" + File.separator + args[0].toLowerCase() + ".yml");
	        config = YamlConfiguration.loadConfiguration(file);
	        StringBuilder x = new StringBuilder();
	        for (int i = 1; i < args.length; i++) {
	          x.append(args[i] + " ");
	        }
	        config.set("ban_reason", x.toString().trim());
	        config.set("ban_sender", sender.getName());
	        try
	        {
	          config.save(file);
	        }
	        catch (Exception e)
	        {
	          e.printStackTrace();
	          sender.sendMessage(prefix + ChatColor.RED + "Player Data could not be saved");
	        }
	        online.setBanned(true);
	        online.kickPlayer(prefix1 + ChatColor.GRAY + "You have been banned by " + sender.getName() + " for " + x.toString().trim());
	        Bukkit.broadcastMessage((prefix1 + DataManager.s(online) + "" + ChatColor.GRAY + " " +  online.getName() + " has been banned by " + DataManager.s(p) + "" + ChatColor.GRAY + " " +  sender.getName() + " for " + x.toString().trim()));
	      }
	    }
	  }
	  if (tag.equalsIgnoreCase("check"))
	  {
	    if (args.length == 0)
	    {
	      sender.sendMessage(prefix + ChatColor.RED + "Invalid usage: /check {username}");
	      return true;
	    }
	    if (args.length == 1)
	    {
	      Player online = Bukkit.getPlayer(args[0]);
	      if (online == null)
	      {
	        OfflinePlayer offline = Bukkit.getOfflinePlayer(args[0]);
	        FileConfiguration config = null;
	        File file = new File("plugins" + File.separator + "bans" + File.separator + "players" + File.separator + offline.getName() + ".yml");
	        config = YamlConfiguration.loadConfiguration(file);
	        String banreason = config.getString("ban_reason");
	        String bannedby = config.getString("ban_sender");
	        sender.sendMessage(ChatColor.RED + offline.getName() + "'s Private Ban Data");
	        if (config.contains("ban_reason")) {
	          sender.sendMessage(ChatColor.GOLD + "Ban Reason: " + ChatColor.RED + banreason);
	        } else {
	          sender.sendMessage(ChatColor.GOLD + "Ban Reason: " + ChatColor.RED + "Player not banned");
	        }
	        if (config.contains("ban_sender")) {
	          sender.sendMessage(ChatColor.GOLD + "Banned by: " + ChatColor.RED + bannedby);
	        } else {
	          sender.sendMessage(ChatColor.GOLD + "Banned by: " + ChatColor.RED + "Player not banned");
	        }
	      }
	    }
	  }
	  if (tag.equalsIgnoreCase("unban"))
	  {
	    if (args.length == 0)
	    {
	      sender.sendMessage(prefix + ChatColor.RED + "/unban {username}");
	      return true;
	    }
	    if (args.length == 1)
	    {
	      OfflinePlayer offline = Bukkit.getOfflinePlayer(args[0]);
	      FileConfiguration config = null;
	      File file = new File("plugins/" + File.separator + "Bans" + File.separator + "players" + File.separator + args[0].toLowerCase() + ".yml");
	      config = YamlConfiguration.loadConfiguration(file);
	      StringBuilder x = new StringBuilder();
	      for (int i = 1; i < args.length; i++) {
	        x.append(args[i] + " ");
	      }
	      try
	      {
	        config.save(file);
	      }
	      catch (Exception e)
	      {
	        e.printStackTrace();
	        sender.sendMessage(prefix + ChatColor.RED + "Player Data could not be saved");
	      }
	      offline.setBanned(false);
	      sender.sendMessage(prefix1 + ChatColor.GRAY + "Player has been unbanned");
	    }
	  }
		  }else{
			  p.sendMessage(prefix + ChatColor.RED + "You dont have the permissions to do this!");
			  return false;
		  }
	  return false;
		  
	}else if(!(sender instanceof Player)){
		  if (tag.equalsIgnoreCase("ban"))
		  {
		    if (args.length < 2)
		    {
		      sender.sendMessage(prefix + ChatColor.RED + "/ban <username> <banreason>");
		      return true;
		    }
		    if (args.length > 1)
		    {
		      Player online = Bukkit.getPlayer(args[0]);
		      if (online == null)
		      {
		        OfflinePlayer offline = Bukkit.getOfflinePlayer(args[0]);
		        FileConfiguration config = null;
		        File file = new File("plugins/" + File.separator + "Bans" + File.separator + "players" + File.separator + args[0].toLowerCase() + ".yml");
		        config = YamlConfiguration.loadConfiguration(file);
		        StringBuilder x = new StringBuilder();
		        for (int i = 1; i < args.length; i++) {
		          x.append(args[i] + " ");
		        }
		        config.set("ban_reason", x.toString().trim());
		        config.set("ban_sender", sender.getName());
		        try
		        {
		          config.save(file);
		        }
		        catch (Exception e)
		        {
		          e.printStackTrace();
		          sender.sendMessage(prefix + ChatColor.RED + "Player Data could not be saved");
		        }
		        offline.setBanned(true);
		        Bukkit.broadcastMessage(prefix1 + ChatColor.GRAY + offline.getName() + " has been banned by " +  sender.getName() + " for " + x.toString().trim());
		      }
		      else
		      {
		        FileConfiguration config = null;
		        File file = new File("plugins/" + File.separator + "Bans" + File.separator + "players" + File.separator + args[0].toLowerCase() + ".yml");
		        config = YamlConfiguration.loadConfiguration(file);
		        StringBuilder x = new StringBuilder();
		        for (int i = 1; i < args.length; i++) {
		          x.append(args[i] + " ");
		        }
		        config.set("ban_reason", x.toString().trim());
		        config.set("ban_sender", sender.getName());
		        try
		        {
		          config.save(file);
		        }
		        catch (Exception e)
		        {
		          e.printStackTrace();
		          sender.sendMessage(prefix + ChatColor.RED + "Player Data could not be saved");
		        }
		        online.setBanned(true);
		        online.kickPlayer(prefix1 + ChatColor.GRAY + "You have been banned by " + sender.getName() + " for " + x.toString().trim());
		        Bukkit.broadcastMessage(prefix1 + DataManager.s(online) + "" + ChatColor.GRAY + " " + online.getName() + " has been banned by " +  ChatColor.GRAY + "" + sender.getName() + " for " + x.toString().trim());
				Main.instance.saveConfig();
				Main.instance.reloadConfig();
		      }
		    }
		  }
		  if (tag.equalsIgnoreCase("check"))
		  {
		    if (args.length == 0)
		    {
		      sender.sendMessage(prefix + ChatColor.RED + "Invalid usage: /check {username}");
		      return true;
		    }
		    if (args.length == 1)
		    {
			    Player t = Bukkit.getServer().getPlayer(args[0]);

		    
		      if (t.isOnline() == false){
		      {
		        OfflinePlayer offline = Bukkit.getOfflinePlayer(args[0]);
		        FileConfiguration config = null;
		        File file = new File("plugins" + File.separator + "bans" + File.separator + "players" + File.separator + offline.getName() + ".yml");
		        config = YamlConfiguration.loadConfiguration(file);
		        String banreason = config.getString("ban_reason");
		        String bannedby = config.getString("ban_sender");
		        sender.sendMessage(ChatColor.RED + offline.getName() + "'s Private Ban Data");
		        if (config.contains("ban_reason")) {
		          sender.sendMessage(ChatColor.GOLD + "Ban Reason: " + ChatColor.RED + banreason);
		        } else {
		          sender.sendMessage(ChatColor.GOLD + "Ban Reason: " + ChatColor.RED + "Player not banned");
		        }
		        if (config.contains("ban_sender")) {
		          sender.sendMessage(ChatColor.GOLD + "Banned by: " + ChatColor.RED + bannedby);
		        } else {
		          sender.sendMessage(ChatColor.GOLD + "Banned by: " + ChatColor.RED + "Player not banned");
		        }
		      }
		    }else{
				Player p = Bukkit.getPlayer(args[0]);
				 FileConfiguration config = null;
			        File file = new File("plugins" + File.separator + "bans" + File.separator + "players" + File.separator + p.getName() + ".yml");
			        config = YamlConfiguration.loadConfiguration(file);
			        String banreason = config.getString("ban_reason");
			        String bannedby = config.getString("ban_sender");
			        sender.sendMessage(ChatColor.RED + p.getName() + "'s Private Ban Data");
			        if (config.contains("ban_reason")) {
			          sender.sendMessage(ChatColor.GOLD + "Ban Reason: "  + ChatColor.RED + banreason);
			        } else {
			          sender.sendMessage(ChatColor.GOLD + "Ban Reason: " + ChatColor.RED + "Player not banned");
			        }
			        if (config.contains("ban_sender")) {
			          sender.sendMessage(ChatColor.GOLD + "Banned by: " + ChatColor.RED + bannedby);
			        } else {
			          sender.sendMessage(ChatColor.GOLD + "Banned by: " + ChatColor.RED + "Player not banned");
			        }
			      }
		    }
		    }
		  }
		  if (tag.equalsIgnoreCase("unban"))
		  {
		    if (args.length == 0)
		    {
		      sender.sendMessage(prefix + ChatColor.RED + "/unban {username}");
		      return true;
		    }
		    if (args.length == 1)
		    {
		      OfflinePlayer offline = Bukkit.getOfflinePlayer(args[0]);
		      FileConfiguration config = null;
		      File file = new File("plugins/" + File.separator + "Bans" + File.separator + "players" + File.separator + args[0].toLowerCase() + ".yml");
		      config = YamlConfiguration.loadConfiguration(file);
		      StringBuilder x = new StringBuilder();
		      for (int i = 1; i < args.length; i++) {
		        x.append(args[i] + " ");
		      }
		      try
		      {
		        config.save(file);
		      }
		      catch (Exception e)
		      {
		        e.printStackTrace();
		        sender.sendMessage(prefix+ ChatColor.RED + "Player Data could not be saved");
		      }
		      config.set("ban_reason", " ");
		      config.set("ban_sender", " ");

		      offline.setBanned(false);
		      sender.sendMessage(prefix1 + ChatColor.GRAY + "Player has been unbanned");
				Main.instance.saveConfig();
				Main.instance.reloadConfig();
		    }
		  }
	
	return false;
	
}

}
