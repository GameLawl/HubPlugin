package com.system.hubsystem.manager;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.system.ranksystem.manager.DataManager;

public class ItemManager {
	
	public static HashMap<String, ItemStack> gadgetHash = new HashMap<String, ItemStack>();
	public static ItemStack shop(){
		ItemStack item = new ItemStack(Material.GOLD_NUGGET);
		ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName(ChatColor.YELLOW +  "Shop" + ChatColor.GRAY + " (Right click)" );
		item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack teleporter(){
		ItemStack item = new ItemStack(Material.COMPASS);
		ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName(ChatColor.GREEN + "Teleporter" + ChatColor.GRAY + " (Right click)");
		item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack gadgets(){
		ItemStack item = new ItemStack(Material.CHEST);
		ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName(ChatColor.DARK_PURPLE + "Gadgets" + ChatColor.GRAY + " (Right click)");
		item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack gadgetsitem(){
		ItemStack item = new ItemStack(Material.CHEST);
		ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName(ChatColor.DARK_PURPLE + "Gadgets");
		item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack lobby(){
		ItemStack item = new ItemStack(Material.EMERALD_BLOCK);
		ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName(ChatColor.BLUE + "Lobby" + ChatColor.GRAY + " (Right click)");
		item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack statics(){
		ItemStack item = new ItemStack(Material.SKULL_ITEM);
		ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName(ChatColor.RED +  "Statics" + ChatColor.GRAY + " (Right click)");
		item.setItemMeta(itemm);
		return item;
	}
	
	
	public static ItemStack LobbyItem(){
		ItemStack Lobby = new ItemStack(Material.EMERALD_BLOCK);
		ItemMeta LobbyMeta = Lobby.getItemMeta();
		LobbyMeta.setDisplayName(ChatColor.GREEN + "Server 1");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Server is up and running!");
		LobbyMeta.setLore(lore);
		Lobby.setItemMeta(LobbyMeta);
		return Lobby;
	}
	
	public static ItemStack LobbyItem1(){
		ItemStack Lobby = new ItemStack(Material.REDSTONE_BLOCK);
		ItemMeta LobbyMeta = Lobby.getItemMeta();
		LobbyMeta.setDisplayName(ChatColor.RED + "Server 2");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "This server isn't online! 0/0");
		LobbyMeta.setLore(lore);
		Lobby.setItemMeta(LobbyMeta);
		return Lobby;
	}
	
	public static ItemStack LobbyItem2(){
		ItemStack Lobby = new ItemStack(Material.REDSTONE_BLOCK);
		ItemMeta LobbyMeta = Lobby.getItemMeta();
		LobbyMeta.setDisplayName(ChatColor.RED + "Server 3");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "This server isn't online! 0/0");
		LobbyMeta.setLore(lore);
		Lobby.setItemMeta(LobbyMeta);
		return Lobby;
	}
	
	public static ItemStack LobbyItem3(){
		ItemStack Lobby = new ItemStack(Material.REDSTONE_BLOCK);
		ItemMeta LobbyMeta = Lobby.getItemMeta();
		LobbyMeta.setDisplayName(ChatColor.RED + "Server 4");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "This server isn't online! 0/0");
		LobbyMeta.setLore(lore);
		Lobby.setItemMeta(LobbyMeta);
		return Lobby;
	}
	
	
	public static ItemStack LobbyItem4(){
		ItemStack Lobby = new ItemStack(Material.REDSTONE_BLOCK);
		ItemMeta LobbyMeta = Lobby.getItemMeta();
		LobbyMeta.setDisplayName(ChatColor.RED + "Server 5");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "This server isn't online! 0/0");
		LobbyMeta.setLore(lore);
		Lobby.setItemMeta(LobbyMeta);
		return Lobby;
	}
	
	public static ItemStack LobbyItem5(){
		ItemStack Lobby = new ItemStack(Material.REDSTONE_BLOCK);
		ItemMeta LobbyMeta = Lobby.getItemMeta();
		LobbyMeta.setDisplayName(ChatColor.RED + "Server 6");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "This server isn't online! 0/0");
		LobbyMeta.setLore(lore);
		Lobby.setItemMeta(LobbyMeta);
		return Lobby;
	}
	
	public static ItemStack LobbyItem6(){
		ItemStack Lobby = new ItemStack(Material.REDSTONE_BLOCK);
		ItemMeta LobbyMeta = Lobby.getItemMeta();
		LobbyMeta.setDisplayName(ChatColor.RED + "Server 7");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "This server isn't online! 0/0");
		LobbyMeta.setLore(lore);
		Lobby.setItemMeta(LobbyMeta);
		return Lobby;
	}
	
	public static ItemStack Mounts(){
		ItemStack Item = new ItemStack(Material.DIAMOND_BARDING);
		ItemMeta itemm = Item.getItemMeta();
		itemm.setDisplayName(ChatColor.BLUE + "Mounts");
		Item.setItemMeta(itemm);
		return Item;
	}
	
	public static ItemStack Pets(){
		ItemStack item = new ItemStack(Material.SADDLE);
		ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName(ChatColor.YELLOW + "Pets");
		item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack Ingame(){
		ItemStack item = new ItemStack(Material.EMERALD);
		ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName(ChatColor.GOLD +  "Ingame-Perks");
		item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack Disguises(){
		ItemStack item = new ItemStack(Material.SKULL_ITEM);
		ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName(ChatColor.RED  + "Disguises");
		item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack PartiesFriends(){
		ItemStack item = new ItemStack(Material.BOOK);
		ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName(ChatColor.WHITE + "Parties & Friends");
		item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack Particles(){
		ItemStack item = new ItemStack(Material.MOB_SPAWNER);
		ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName(ChatColor.DARK_PURPLE + "Particles");
		item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack FlowerPower(){
		ItemStack item = new ItemStack(38, 1, (byte) 7);
		ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.DARK_PURPLE + "FlowerPower");
		item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack Slapper(){
		ItemStack item = new ItemStack(44, 1, (byte) 3);
		ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.DARK_GRAY + "Slapper");
		item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack Fireball(){
		ItemStack item = new ItemStack(Material.FIREBALL);
		ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.RED + "Fireball");
		item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack FireWork(){
		ItemStack item = new ItemStack(Material.FIREWORK);
		ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.YELLOW + "Firework");
		item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack Wand(Player p){
		ItemStack item = new ItemStack(Material.STICK);
		ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.DARK_GREEN + "Wand " + ChatColor.GRAY + "(Right click)");
	    ArrayList<String> lore = new ArrayList<String>();
	    
	    lore.add(ChatColor.BLUE + "Selected Gadget: " + ChatColor.GRAY + "" + DataManager.gadget.get(p.getName()).toUpperCase());
	    itemm.setLore(lore);
		item.setItemMeta(itemm);
		ItemManager.gadgetHash.put(p.getName(), item);
		return item;
	}
	
	public static ItemStack Hub(){
		ItemStack item = new ItemStack(Material.BOOKSHELF);
		ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.GOLD + "Hub");
		item.setItemMeta(itemm);
		return item;
	}
	

	public static ItemStack Games(){
		ItemStack item = new ItemStack(Material.COMPASS);
		ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.RED + "Lobby Games");
		item.setItemMeta(itemm);
		return item;
	}
	
}
