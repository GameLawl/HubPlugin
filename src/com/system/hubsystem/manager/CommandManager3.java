package com.system.hubsystem.manager;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.system.ranksystem.Main;
import com.system.ranksystem.manager.PermissionManager;


public class CommandManager3 implements CommandExecutor{

	public static String prefix = ChatColor.GREEN + "COMMAND>> ";
	public static String prefix1 = ChatColor.RED + "BROADCAST>> ";	
	
	  @SuppressWarnings("static-access")
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	  {
	    if (commandLabel.equalsIgnoreCase("stopbroadcast"))
	    {
	      if (Main.instance.running == 1)
	      {
	        Player player = (Player)sender;
	        if(PermissionManager.hasPermission(player, "admin")){
	        Bukkit.getServer().getScheduler().cancelTask(Main.instance.tid);

	        player.sendMessage("Cancelled broadcasts.");
	        Main.instance.running = 0;
	        }else{
	        	player.sendMessage(prefix + ChatColor.RED + "You are not allowed to do this!");
	        	return false;
	        	
	        }
	      }
	      else
	      {
	        Player player = (Player)sender;
	        player.sendMessage(prefix1 + ChatColor.RED +  "There aren't any broadcasted running at the moment!");
	      }
	    }
	    else if (commandLabel.equalsIgnoreCase("startbroadcast")) {
	      if (Main.instance.running == 1)
	      {
	    	  
		        Player player = (Player)sender;
		        if(PermissionManager.hasPermission(player, "admin")){

	    
			        player.sendMessage(prefix1 + ChatColor.RED +  "There are still some broadcastings running!");
		        }else{
		        	player.sendMessage(prefix + ChatColor.RED + "You are not allowed to do this!");

			    	  return false;
		        }
	    	  
	      }
	      else
	      {
		        Player player = (Player)sender;

	    	  if(PermissionManager.hasPermission(player, "admin")){

	    	  Main.instance.tid = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.instance, new Runnable()
	        {
	          public void run()
	          {
	            Bukkit.broadcastMessage("plugins/Broadcast/messages.txt");
	          }
	        }, 0L, Main.instance.interval * 20L);
	        player.sendMessage(prefix1 + ChatColor.GRAY + "Started broadcast!");
	        Main.instance.running = 1;
	      }else{
	        	player.sendMessage(prefix + ChatColor.RED + "You are not allowed to do this!");

	    	  return false;
	    	  
	      }
	      }
	    }
	    return false;
	  }
}
