package com.system.commandsystem.executors;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.system.ranksystem.manager.PermissionManager;

public class Vanish implements CommandExecutor {

	public static ArrayList<String> vanished = new ArrayList<String>();
	public static String prefix = ChatColor.GREEN + "COMMAND>> ";
	public static String prefix1 = ChatColor.DARK_GREEN + "VANISH>> ";

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel,
			String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(prefix + ChatColor.RED + "You cannot vanish!");
			return true;
		}
		
		Player p = (Player) sender;
		
		if (cmd.getName().equalsIgnoreCase("vanish")) {
			if(args.length == 0){
				p.sendMessage(prefix + ChatColor.RED + "Invalid usage: /vanish {on/off}");
				return false;
			}
			if(PermissionManager.hasPermission(p, "admin") == true){
			if (!vanished.contains(p.getName())) {
				for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
					pl.hidePlayer(p);
				}
				vanished.add(p.getName());
				p.sendMessage(prefix1 + ChatColor.GRAY + "You have been vanished!");
				return true;
			}
			else {
				for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
					pl.showPlayer(p);
				}
				vanished.remove(p.getName());
				p.sendMessage(prefix1 + ChatColor.GRAY + "You are no longer vanished!");
				return true;
			}
		}else{
			p.sendMessage(prefix + ChatColor.RED + "You are not allowed to do this!");

			return false;
		}
			
		}
		return false;
	}
	
	
	
	

}
