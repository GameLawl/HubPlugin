package com.system.commandsystem.executors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.system.ranksystem.manager.DataManager;
import com.system.ranksystem.manager.PermissionManager;

public class Kick implements CommandExecutor {
	
	public static String prefix = ChatColor.GREEN + "COMMAND>> ";
	public static String prefix1 = ChatColor.AQUA + "KICK>> ";

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel,
			String[] args) {
		if(sender instanceof Player){
			
		
		Player p = (Player) sender;
		
		if (cmd.getName().equalsIgnoreCase("kick")) {
			if(PermissionManager.hasPermission(p, "moderator" )== true){
			if(args.length == 0) {
				sender.sendMessage(prefix + ChatColor.RED + "Usage: /kick {username}");
				return true;
			}
			Player target = Bukkit.getServer().getPlayer(args[0]);
			if (target == null) {
				sender.sendMessage(prefix + ChatColor.RED + "Could not find player " +  args[0] + "!");
				return true;
			}
			target.kickPlayer(prefix1 + ChatColor.GRAY + "You have been kicked!");
			Bukkit.getServer().broadcastMessage(prefix1 + ChatColor.GRAY + "Player " + DataManager.s(target) + " " + ChatColor.GRAY +  target.getName() + " has been kicked by " + DataManager.s(p) + "" +ChatColor.GRAY + " " + p.getName() + "!");
			}else{
				p.sendMessage(prefix + ChatColor.RED + "You are not allowed to do this!");

				return false;
			}
			}
		}else
		
			if(!(sender instanceof Player)){
				
				
				if (cmd.getName().equalsIgnoreCase("kick")) {
					if(args.length == 0) {
						sender.sendMessage(prefix + ChatColor.RED + "Usage: /kick {username}");
						return true;
					}
					Player target = Bukkit.getServer().getPlayer(args[0]);
					if (target == null) {
						sender.sendMessage(prefix + ChatColor.RED + "Could not find player " + args[0] + "!");
						return true;
					}
					target.kickPlayer(prefix1 + ChatColor.GRAY + "You have been kicked!");
					Bukkit.getServer().broadcastMessage(prefix1 + ChatColor.GRAY + "" + DataManager.s(target) + " " + ChatColor.GRAY +target.getName() + " has been kicked by " +  ChatColor.GRAY + sender.getName() + "!");
				}
				
					
				
		}
		return false;
		
	}
	
	

}
