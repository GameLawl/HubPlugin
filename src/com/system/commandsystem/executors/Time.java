package com.system.commandsystem.executors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.system.ranksystem.manager.DataManager;
import com.system.ranksystem.manager.PermissionManager;

public class Time implements CommandExecutor {
	public static String prefix = ChatColor.GREEN + "COMMAND>> ";
	public static String prefix1 = ChatColor.DARK_GRAY + "TIME>> ";

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if(sender instanceof Player){
			Player p = (Player) sender;
			if(PermissionManager.hasPermission(p, "admin") == true){
			if(args.length == 0){
				p.sendMessage(prefix + ChatColor.RED + "Invalid usage: /time {time}");
				return false;
			}
			long arg1 = Long.valueOf(args[0]);

		p.getWorld().setTime(arg1); 
		Bukkit.broadcastMessage(prefix1 + ChatColor.GRAY + "The time has been set to " + arg1 + " by " + DataManager.s(p) + ChatColor.GRAY + "" + " " + p.getName());
		
			}else{
				p.sendMessage(prefix + ChatColor.RED + " You are not allowed to do this!");
				return false;
			}
		}else{
			if(args.length == 0){
				sender.sendMessage(prefix + ChatColor.RED + "Invalid usage: /time {time}");
				return false;
			}
			long arg1 = Long.valueOf(args[0]);

		for(Player p : Bukkit.getOnlinePlayers()){
			p.getWorld().setTime(arg1);
		}
		Bukkit.broadcastMessage(prefix1 + ChatColor.GRAY + "The time has been set to " + arg1 + " by "  + ChatColor.GRAY + "" + sender);
		
			
		}
	    
		return false;
	}

}
