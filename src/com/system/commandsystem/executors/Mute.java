package com.system.commandsystem.executors;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.system.ranksystem.manager.DataManager;
import com.system.ranksystem.manager.PermissionManager;

public class Mute implements CommandExecutor {
	
	public static ArrayList<String> mute = new ArrayList<String>();
	public static String prefix = ChatColor.GREEN + "COMMAND>> ";
	public static String prefix1 = ChatColor.BLUE + "HUB>> ";


	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String tag,
			String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(prefix + ChatColor.RED + "Player command only!");
			return false;
		}
		Player player = (Player) sender;
		
		if(cmd.getName().equalsIgnoreCase("mute")) {
			if(PermissionManager.hasPermission(player, "support")){
			if(args.length == 0) {
				player.sendMessage(prefix + ChatColor.RED + "Invalid usage: /mute {username}");
				return false;
			}
			if(args.length == 1) {
				Player online = Bukkit.getPlayer(args[0]);
				if(online.isOnline() == false) {
					OfflinePlayer offline = Bukkit.getOfflinePlayer(args[0]);
					mute.add(offline.getName());
					player.sendMessage(prefix1 + ChatColor.RED + "You have muted " +  offline);
				} else {
					mute.add(online.getName());
					player.sendMessage(prefix1 + ChatColor.GRAY + "You have muted " + DataManager.s(online) + "" + ChatColor.GRAY + " "+  online.getName());
					online.sendMessage(prefix1 + ChatColor.GRAY + "You have been muted " + DataManager.s(online) + "" + ChatColor.GRAY + " "+  online.getName());
				}
			}
			}else{
				player.sendMessage(prefix + ChatColor.RED + "You are not allowed to do this!");

				return false;
			}
	
	
		}else 	if(cmd.getName().equalsIgnoreCase("unmute")) {
			if(args.length == 0) {
				player.sendMessage(prefix +ChatColor.RED + "Invalid usage: /unmute {username}");
				return false;
			}
			if(PermissionManager.hasPermission(player, "support")){
			if(args.length == 1) {
				Player online = Bukkit.getPlayer(args[0]);
				if(online.isOnline() == false) {
			        OfflinePlayer offline = Bukkit.getOfflinePlayer(args[0]);
					mute.remove(offline.getName());
					player.sendMessage(prefix1 + ChatColor.RED + "You have unmuted " + DataManager.s(online) + "" + ChatColor.GRAY + " "+  offline);

				} else {
					mute.remove(online.getName());
					player.sendMessage(prefix1 + ChatColor.GRAY + "You have unmuted " + DataManager.s(online) + "" + ChatColor.GRAY + " "+  online.getName());
					online.sendMessage(prefix1 + ChatColor.RED + "You have been unmuted");
				}
			}
	    }else{
			player.sendMessage(prefix + ChatColor.RED + "You are not allowed to do this!");

			return false;
		}
		}
		return false;		
		
  

	}
}
