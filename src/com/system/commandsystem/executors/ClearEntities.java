package com.system.commandsystem.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.system.ranksystem.manager.PermissionManager;

public class ClearEntities implements CommandExecutor {

	public static String prefix = ChatColor.GREEN + "COMMAND>> ";
	public static String prefix1 = ChatColor.BLUE + "HUB>> ";
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		// TODO Auto-generated method stub
		if(sender instanceof Player){
			Player p = (Player) sender;
			if(PermissionManager.hasPermission(p, "admin") == true){
				if(args.length == 0){
					p.sendMessage(prefix + ChatColor.RED + "Invalid usage: /clearentities {number}");

					return false;
				}
					double arg1 = Double.valueOf(args[0]);
					p.sendMessage(prefix1 + ChatColor.GRAY + "Removed all entities in a radius of " + arg1 + "!");

					for(Entity e : p.getNearbyEntities(arg1, arg1, arg1)){
						Entity e1 = (Entity) e;

						if(e1 instanceof Item){
							Item e11 = (Item) e1;
							e11.remove();
						}
						if(e1 instanceof LivingEntity){
							LivingEntity living = (LivingEntity) e1;
							if(living instanceof Player){
								
								return true;
							
					
						}else{
							LivingEntity liv = (LivingEntity) living;
							liv.remove();

						}
					}
				}
			}else{
				p.sendMessage(prefix + ChatColor.RED + "You are not allowed to do this!");

				return false;
			}
		}else{
			sender.sendMessage(prefix + ChatColor.RED + "Only a player can use this command!");
			return false;
			
		}
		return false;
	}

}
