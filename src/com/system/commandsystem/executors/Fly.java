package com.system.commandsystem.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.system.hubsystem.manager.ParticleManager;
import com.system.ranksystem.manager.DataManager;
import com.system.ranksystem.manager.PermissionManager;

public class Fly implements CommandExecutor {
	public static String prefix = ChatColor.GREEN + "COMMAND>> ";
	public static String prefix1 = ChatColor.BLUE + "HUB>> ";
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
			if(!(sender instanceof Player)){
				sender.sendMessage(prefix +  ChatColor.RED + "Only a player can perform this command!");
				return false;
				
			}
			
			Player p = (Player) sender;
			if(args.length == 0){
				p.sendMessage(prefix + ChatColor.RED + "Invalid usage: /fly {on/off}");
				return false;
				
			}
			String arg1 = String.valueOf(args[0]);
			if(PermissionManager.hasPermission(p, "vip") == true){
				switch(arg1.toLowerCase()){
				case("on"):
				if(DataManager.flying.contains(p.getName())){
					p.sendMessage(prefix1 + ChatColor.RED + "You are already flying!");
					return false;
				}else if(!(DataManager.flying.contains(p.getName()))){
					DataManager.flying.add(p.getName());
					p.setAllowFlight(true);
					p.setFlying(true);
					p.sendMessage(prefix1 + ChatColor.GRAY + "Enjoy flying!");
					ParticleManager.onMethod(p, "flying", "on");

					return true;
				}
				break;
				case("off"):
					if(!(DataManager.flying.contains(p.getName()))){
						p.sendMessage(prefix1 + ChatColor.RED + "You aren't flying!");
						return false;
					}else if(DataManager.flying.contains(p.getName())){
						p.setAllowFlight(false);
						p.setFlying(false);
						DataManager.flying.remove(p.getName());
						p.sendMessage(prefix1 + ChatColor.GRAY + "You disabled flying!");
						ParticleManager.onMethod(p, "flying", "off");

						return true;
					}
				}
			}else{
				p.sendMessage(prefix + ChatColor.RED + "You are not allowed to do this!");

				return false;
			}
			
			
		return false;
	}

}
