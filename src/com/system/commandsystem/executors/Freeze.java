package com.system.commandsystem.executors;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.system.ranksystem.manager.DataManager;
import com.system.ranksystem.manager.PermissionManager;

public class Freeze implements CommandExecutor {
	
	public static ArrayList<String> frozen = new ArrayList<String>();
	public static String prefix = ChatColor.GREEN + "COMMAND>> ";
	public static String prefix1 = ChatColor.BLUE + "HUB>> ";
	

public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
	Player player = (Player) sender;
	
	if(cmd.getName().equalsIgnoreCase("freeze")) {
		if(PermissionManager.hasPermission(player, "admin") == true) {
			if(args.length == 0) {
				player.sendMessage(prefix + ChatColor.RED + "You must specify a player!");
				return true;
			    }
			
			    Player t = Bukkit.getServer().getPlayer(args[0]);
			    
			    if(t == null) {
			    	player.sendMessage(prefix + ChatColor.RED + "Player not online!");
			    	return true;
			    }
			    
			    if(frozen.contains(t.getName())) {
			    	frozen.remove(t.getName());
			    	player.sendMessage(prefix1 + DataManager.s(t) + "" + ChatColor.GRAY +  " " +  t.getName() + " is no longer frozen.");
			    	t.sendMessage(prefix1 + ChatColor.GRAY + "You have been unfrozen by " + DataManager.s(player) + ""  + ChatColor.GRAY + " "+ player.getName() + ".");
			    	return true;
			    } else {
			    	frozen.add(t.getName());
			    	frozen.remove(player.getName());
			    	player.sendMessage(prefix1 + DataManager.s(t) + "" + ChatColor.GRAY +  " " + t.getName() + " is now frozen.");
			    	t.sendMessage(prefix1 + ChatColor.GRAY + "You have been frozen by " + DataManager.s(player) + ""  + ChatColor.GRAY + " "+ player.getName() + ".");
			    	return true;
			    }
			} else {
				player.sendMessage(prefix + ChatColor.RED + "You do not have permission to run this command.");
				return true;
			}
		}
	return false;
	}
}
