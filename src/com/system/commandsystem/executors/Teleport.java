package com.system.commandsystem.executors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.system.ranksystem.manager.DataManager;
import com.system.ranksystem.manager.PermissionManager;

public class Teleport implements CommandExecutor {
	public static String prefix = ChatColor.GREEN + "COMMAND>> ";
	public static String prefix1 = ChatColor.DARK_AQUA + "TELEPORT>> ";
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if(sender instanceof Player){
			Player p = (Player) sender;
			if(cmd.getName().equalsIgnoreCase("tp")){
				if(PermissionManager.hasPermission(p, "moderator") == true){
			if(args.length == 0){
				p.sendMessage(prefix + ChatColor.RED + "Invalid usage: /tp {player}");
				return false;
			}
			Player target = Bukkit.getServer().getPlayer(args[0]);
			if (target == null) {
				sender.sendMessage(prefix + ChatColor.RED + "Could not find player " +  args[0] + "!");
				return true;
			}
			if(DataManager.tptoggled.contains(p.getName())){
				p.sendMessage(prefix1 + ChatColor.GRAY + "You cannot teleport to " + DataManager.s(target) + "" + ChatColor.GRAY + " " + target.getName());
				return false;
			}else{
			Location loc = new Location(target.getWorld(),target.getLocation().getX(), target.getLocation().getY(), target.getLocation().getZ(), target.getLocation().getYaw(), target.getLocation().getPitch());
			float yaw = target.getLocation().getYaw();
			float pitch = target.getLocation().getPitch();
			loc.setYaw(yaw);
			loc.setPitch(pitch);
			p.getLocation().setYaw(yaw);
			p.getLocation().setPitch(pitch);
			p.teleport(loc);
			p.sendMessage(prefix1 + ChatColor.GRAY + "You have been teleport to " + DataManager.s(target) + "" + ChatColor.GRAY + " " + target.getName());
			}
				}else{
					sender.sendMessage(prefix + ChatColor.RED + " You are not allowed to do this!");
					return false;
				}
			
			}
		}else{
			sender.sendMessage(prefix +  "Only a player can perform this command!");

			return false;
		}
			
		
		
		return false;
	}

}

