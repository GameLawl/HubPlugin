package com.system.commandsystem.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Help implements CommandExecutor {
	
	public static String prefix = ChatColor.GREEN + "COMMAND>> ";

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if(cmd.getName().equalsIgnoreCase("help")){
			sender.sendMessage(prefix + ChatColor.GRAY + "There is no such thing as '/help'!");
			return false;
		}
		if(cmd.getName().equalsIgnoreCase("pl")){
			sender.sendMessage(prefix + ChatColor.GRAY + "There is no such thing as '/pl'!");
			return false;
		}
		if(cmd.getName().equalsIgnoreCase("plugins")){
			sender.sendMessage(prefix + ChatColor.GRAY + "There is no such thing as '/plugins'!");
			return false;
		}
		
		if(cmd.getName().equalsIgnoreCase("tell")){
			sender.sendMessage(prefix + ChatColor.GRAY + "There is no such thing as '/tell'!");
			return false;
		}
		
		if(cmd.getName().equalsIgnoreCase("msg")){
			sender.sendMessage(prefix + ChatColor.GRAY + "There is no such thing as '/msg'!");
			return false;
		}
		
		if(cmd.getName().equalsIgnoreCase("list")){
			sender.sendMessage(prefix + ChatColor.GRAY + "There is no such thing as '/list'!");
			return false;
		}
	
		if(cmd.getName().equalsIgnoreCase("give")){
			sender.sendMessage(prefix + ChatColor.GRAY + "There is no such thing as '/give'!");
			return false;
		}
		
		if(cmd.getName().equalsIgnoreCase("gamemode")){
			sender.sendMessage(prefix + ChatColor.GRAY + "There is no such thing as '/gamemode'!");
			return false;
		}
		
		if(cmd.getName().equalsIgnoreCase("?")){
			sender.sendMessage(prefix + ChatColor.GRAY + "There is no such thing as '/help'!");
			return false;
		}
		
		return false;
	}

}
