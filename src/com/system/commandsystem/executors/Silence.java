package com.system.commandsystem.executors;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.system.ranksystem.manager.PermissionManager;

public class Silence implements CommandExecutor {
	
	public static boolean chat = false;
	public static ArrayList<String> worlds = new ArrayList<String>();
	public static String prefix = ChatColor.GREEN + "COMMAND>> ";
	public static String prefix1 = ChatColor.BLUE + "HUB>> ";	
	

	@SuppressWarnings("static-access")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("silence")){
			if(sender instanceof Player){
				Player p = (Player) sender;
				if(PermissionManager.hasPermission(p, "admin") == true){
					if(args.length == 0){
						sender.sendMessage(prefix + ChatColor.RED + "Invalid usage: /silence {on/off}");
						return false;
					}
					String arg1 = String.valueOf(args[0]);
					switch(arg1.toUpperCase()){
					case("ON"):
					this.chat = true;
					Bukkit.getServer().broadcastMessage(prefix1 + " The chat has been disabled!");
					break;
					case("OFF"):
						this.worlds.removeAll(this.worlds);
					this.chat = false;
					Bukkit.getServer().broadcastMessage(prefix1 + " The chat has been enabled!");
					break;
					}
				}else{
					p.sendMessage(prefix + ChatColor.RED + " You are not allowed to do this!");
					return false;
				}
			}else if(!(sender instanceof Player)){
				if(args.length == 0){
					sender.sendMessage(prefix + ChatColor.RED + "Invalid usage: /silence {on/off}");
					return false;
				}
				String arg1 = String.valueOf(args[0]);
				switch(arg1.toUpperCase()){
				case("ON"):
				this.chat = true;
				Bukkit.getServer().broadcastMessage(prefix1 + " The chat has been disabled!");
				break;
				case("OFF"):
					this.worlds.removeAll(this.worlds);
				this.chat = false;
				Bukkit.getServer().broadcastMessage(prefix1 + " The chat has been enabled!");
				break;
				}
			}else{
				sender.sendMessage(prefix + ChatColor.RED + " You are not allowed to do this!");
				return false;
			}
			}
			
			
				
			

	
		

		
	

		return false;	
	}
	

	
	

}
