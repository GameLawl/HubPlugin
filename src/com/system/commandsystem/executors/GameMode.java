package com.system.commandsystem.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.system.ranksystem.manager.DataManager;
import com.system.ranksystem.manager.PermissionManager;

public class GameMode implements CommandExecutor {
	public static String prefix = ChatColor.GREEN + "COMMAND>> ";
	public static String prefix1 = ChatColor.BLUE + "HUB>> ";

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		// TODO Auto-generated method stub
		if(sender instanceof Player){
			Player p = (Player) sender;
			if(cmd.getName().equalsIgnoreCase("gm")){
				if(args.length == 0){
					p.sendMessage(prefix + ChatColor.RED + "Invalid usage: /gamemode {c,s,a}");
					return false;
				}
				String arg1 = String.valueOf(args[0]);

				if(PermissionManager.hasPermission(p, "buildteam") == true){
					switch(arg1.toLowerCase()){
					case("c"):
						p.setGameMode(org.bukkit.GameMode.CREATIVE);

						break;
					case("a"):
						p.setGameMode(org.bukkit.GameMode.ADVENTURE);
					if(DataManager.flying.contains(p.getName())){
						p.setAllowFlight(true);
						p.setFlying(true);
					}

						break;
					case("s"):
						p.setGameMode(org.bukkit.GameMode.SURVIVAL);
					if(DataManager.flying.contains(p.getName())){
						p.setAllowFlight(true);
						p.setFlying(true);
					}

						break;
					case("1"):
						p.setGameMode(org.bukkit.GameMode.CREATIVE);

						break;
					case("0"):
						p.setGameMode(org.bukkit.GameMode.SURVIVAL);
						if(DataManager.flying.contains(p.getName())){
							p.setAllowFlight(true);
							p.setFlying(true);
						}

						break;
					case("2"):
						p.setGameMode(org.bukkit.GameMode.ADVENTURE);
					if(DataManager.flying.contains(p.getName())){
						p.setAllowFlight(true);
						p.setFlying(true);
					}

						break;
					}
				}else{
					p.sendMessage(prefix + ChatColor.RED + "You are not allowed to perform this command!");
					return false;
				}
					
			}
		}else if(!(sender instanceof Player)){
			sender.sendMessage(prefix +  "Only a player can perform this command!");
			return false;
		}
		return false;
	}

}
