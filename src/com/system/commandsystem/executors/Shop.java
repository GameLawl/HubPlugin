package com.system.commandsystem.executors;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.system.ranksystem.manager.AutoRankManager;

public class Shop implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		// TODO Auto-generated method stub
		if(sender instanceof Player){
			return false;
		}else{
			String arg2 = String.valueOf(args[1]);
			String arg1 = String.valueOf(args[0]);

			AutoRankManager.onCheckShop(arg1, arg2);
			
		}
		return false;
		
	}

}
