package com.system.ranksystem;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.sql.Connection;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.plugin.java.JavaPlugin;

import com.system.bansystem.CommandManager2;
import com.system.commandsystem.executors.ClearEntities;
import com.system.commandsystem.executors.Fly;
import com.system.commandsystem.executors.Freeze;
import com.system.commandsystem.executors.GameMode;
import com.system.commandsystem.executors.Help;
import com.system.commandsystem.executors.Kick;
import com.system.commandsystem.executors.Mute;
import com.system.commandsystem.executors.Shop;
import com.system.commandsystem.executors.Silence;
import com.system.commandsystem.executors.Spawn;
import com.system.commandsystem.executors.Teleport;
import com.system.commandsystem.executors.Time;
import com.system.commandsystem.executors.Vanish;
import com.system.hubsystem.manager.BarManager;
import com.system.hubsystem.manager.CommandManager3;
import com.system.inventorysystem.inventories.GadgetInventory;
import com.system.inventorysystem.inventories.LobbyInventory;
import com.system.inventorysystem.inventories.ShopInventory;
import com.system.inventorysystem.inventories.StaticsInventory;
import com.system.inventorysystem.inventories.TeleportInventory;
import com.system.ranksystem.listener.BlockListener;
import com.system.ranksystem.listener.ChatListener;
import com.system.ranksystem.listener.DamageListener;
import com.system.ranksystem.listener.FoodListener;
import com.system.ranksystem.listener.InteractListener;
import com.system.ranksystem.listener.InventoryInteractListener;
import com.system.ranksystem.listener.ItemListener;
import com.system.ranksystem.listener.JoinListener;
import com.system.ranksystem.listener.MoveListener;
import com.system.ranksystem.listener.QuitListener;
import com.system.ranksystem.listener.WorldListener;
import com.system.ranksystem.manager.CoinCommandManager;
import com.system.ranksystem.manager.CommandManager;
import com.system.ranksystem.manager.ConfigManager;
import com.system.ranksystem.manager.DataManager;
import com.system.ranksystem.manager.PermissionManager;
import com.system.ranksystem.manager.PrefixManager;
import com.system.ranksystem.manager.RankManager;

public class Main extends JavaPlugin {

	public static Main instance;
	public static int currentLine = 0;
	public static int tid = 0;
	public static int running = 1;
	public static long interval = 120l;
	public static Connection c = null;
	
	@Override
	public void onEnable() {
		getServer().getMessenger().registerOutgoingPluginChannel(this, 
				"BungeeCord");

		instance = this;
		
		for (Player p : Bukkit.getOnlinePlayers()) {
			String r = "ranksystem.rank." + p.getName();
		//	BarManager.showBarChanging(p);

			String rank = Main.instance.getConfig().getString(r);
			String c = "ranksystem.coins." + p.getName();

			int coins = Main.instance.getConfig().getInt(c);
			RankManager.onMethod(p, rank, "add");
			PrefixManager.onMethod(p, rank);
			PermissionManager.onMethod(p, rank, "add");
			StaticsInventory.onMethod(p, "create");
			TeleportInventory.onMethod(p, "create");
			ShopInventory.onMethod(p, "create");		
			GadgetInventory.onMethod(p, "create");		
			LobbyInventory.onMethod(p, "create");	
			ConfigManager.onConfigCoins(p, coins, "add");
		}
		
		
		Bukkit.getPluginManager().registerEvents(new JoinListener(), this);
		Bukkit.getPluginManager().registerEvents(new BlockListener(), this);
		Bukkit.getPluginManager().registerEvents(new MoveListener(), this);
		Bukkit.getPluginManager().registerEvents(new DamageListener(), this);
		Bukkit.getPluginManager().registerEvents(new FoodListener(), this);
		Bukkit.getPluginManager().registerEvents(new DamageListener(), this);
		Bukkit.getPluginManager().registerEvents(new ItemListener(), this);
		Bukkit.getPluginManager().registerEvents(new InteractListener(), this);
		Bukkit.getPluginManager().registerEvents(new InventoryInteractListener(), this);
		Bukkit.getPluginManager().registerEvents(new QuitListener(), this);
		Bukkit.getPluginManager().registerEvents(new WorldListener(), this);

		Bukkit.getPluginManager().registerEvents(new ChatListener(), this);
		String[] ranks = { "default, vip, donator, jrsupport, supporttrail, support, headsupport, jrmoderator, moderatortrail, moderator, headmoderator, jrbuildteam, buildteamtrail, buildteam, buildteamleader, youtube, jradmin, admintrail, admin, headadmin, jrdev, devtrail, dev, headdev, owner" };
		DataManager.list.put("test", ranks);
		getCommand("rank").setExecutor(new CommandManager());
		getCommand("ban").setExecutor(new CommandManager2());
		getCommand("check").setExecutor(new CommandManager2());
		getCommand("unban").setExecutor(new CommandManager2());
		getCommand("stopbroadcast").setExecutor(new CommandManager3());
		getCommand("startbroadcast").setExecutor(new CommandManager3());
		getCommand("fly").setExecutor(new Fly());
		getCommand("freeze").setExecutor(new Freeze());
		getCommand("gm").setExecutor(new GameMode());
		getCommand("silence").setExecutor(new Silence());
		getCommand("mute").setExecutor(new Mute());
		getCommand("unmute").setExecutor(new Mute());
		getCommand("kick").setExecutor(new Kick());
		getCommand("vanish").setExecutor(new Vanish());
		getCommand("tp").setExecutor(new Teleport());
		getCommand("help").setExecutor(new Help());
		getCommand("pl").setExecutor(new Help());
		getCommand("plugins").setExecutor(new Help());
		getCommand("tell").setExecutor(new Help());
		getCommand("msg").setExecutor(new Help());
		getCommand("give").setExecutor(new Help());
		getCommand("list").setExecutor(new Help());
		getCommand("gamemode").setExecutor(new Help());
		getCommand("setspawn").setExecutor(new Spawn());
		getCommand("spawn").setExecutor(new Spawn());
		getCommand("time").setExecutor(new Time());
		getCommand("?").setExecutor(new Help());
		getCommand("clearentities").setExecutor(new ClearEntities());
		getCommand("coins").setExecutor(new CoinCommandManager());
		getCommand("shop").setExecutor(new Shop());


		tid = Bukkit.getScheduler().scheduleSyncRepeatingTask(this,
				new Runnable() {
					@SuppressWarnings("static-access")
					public void run() {
						try {
							Main.instance
									.broadcastMessage("plugins/Broadcast/messages.txt");
						} catch (IOException localIOException) {
						}
					}
				}, 0L, interval * 20L);
		
		/////////////////////////////
		
		/*	try {
				c = MySQL.openConnection();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		*/
	}

	@SuppressWarnings("resource")
	public static void broadcastMessage(String fileName) throws IOException {
		FileInputStream fs = new FileInputStream(fileName);

		BufferedReader br = new BufferedReader(new InputStreamReader(fs));
		for (int i = 0; i < currentLine; i++) {
			br.readLine();
		}
		String line = br.readLine();
		String line2 = line.replace("&", "�");

		Bukkit.getServer().broadcastMessage(
				ChatColor.GOLD + "" + ChatColor.ITALIC + "** "
						+ ChatColor.WHITE + line2);

		LineNumberReader lnr = new LineNumberReader(new FileReader(new File(
				fileName)));
		lnr.skip(9223372036854775807L);
		int lastLine = lnr.getLineNumber();
		if (currentLine + 1 == lastLine + 1) {
			currentLine = 0;
		} else {
			currentLine += 1;
		}
	}

	@Override
	public void onDisable() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			String r = "ranksystem.rank." + p.getName();

			String rank = Main.instance.getConfig().getString(r);
			RankManager.onMethod(p, rank, "add");
			PrefixManager.onMethod(p, rank);
			PermissionManager.onMethod(p, rank, "add");
			
		}
		saveConfig();
		
		//try {
	//		c.close();
	//	} catch (SQLException e) {
	//		// TODO Auto-generated catch block
	//		e.printStackTrace();
	//	}
	}
}
