package com.system.ranksystem.manager;

import org.bukkit.entity.Player;

import com.system.ranksystem.Main;

public class AutoRankManager {

	public static void onCheck(Player p){
		if(DataManager.prefix.containsKey(p.getName())){
			System.out.println("Data Checked for Player " + p.getName() + " everything is up to date now!");

		}else{
			String r = "ranksystem.rank." + p.getName() ;
			Main.instance.getConfig().addDefault(r, "default");
			Main.instance.getConfig().options().copyDefaults(false);

		String rank = Main.instance.getConfig().getString(r);
		System.out.println(rank);

			RankManager.onMethod(p, rank, "add");
				PrefixManager.onMethod(p, rank);
				PermissionManager.onMethod(p, rank, "add");
				System.out.println("Data Checked for Player " + p.getName() + " everything is up to date now!");
		}
	}
	
	public static void onCheckShop(String pname, String i){
		
			String r = "ranksystem.rank." + pname ;
			Main.instance.getConfig().set(r, i);
			Main.instance.saveConfig();
			Main.instance.reloadConfig();
		String rank = Main.instance.getConfig().getString(r);

		System.out.println("Rank add for "  + pname + " everything is up to date now!");

		
	}
}
