package com.system.ranksystem.manager;


import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class RankManager {

	public static void onMethod(Player p, String rank, String m){
		DataManager.rank.remove(p.getName());

		switch(m.toString().toUpperCase()){
		case("ADD"):
			onAddRank(p, rank);
			break;
		case("REMOVE"):
			break;
		case("LOGIN"):
			break;
		}
	}
	
	private static void onAddRank(Player p, String rank){
		DataManager.rank.put(p.getName(), rank);
	}
	
	public static void onMethodOffline(OfflinePlayer p, String rank, String m){
		DataManager.rank.remove(p.getName());

		switch(m.toString().toUpperCase()){
		case("ADD"):
			onAddRank1(p, rank);
			break;
		case("REMOVE"):
			break;
		case("LOGIN"):
			break;
		}
	}
	
	private static void onAddRank1(OfflinePlayer p, String rank){
		DataManager.rank.put(p.getName(), rank);
	}
}
