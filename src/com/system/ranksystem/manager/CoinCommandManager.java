package com.system.ranksystem.manager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.system.hubsystem.manager.ScoreboardManager123;
import com.system.ranksystem.Main;

public class CoinCommandManager implements CommandExecutor {

	
	public static String prefix = ChatColor.GREEN + "COMMAND>> ";
	public static String prefix1 = ChatColor.BLUE + "HUB>> ";	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		// TODO Auto-generated method stub
		if(sender instanceof Player){
			Player p = (Player) sender;
			if(PermissionManager.hasPermission(p, "admin")){
			 if (args.length == 0)
			    {
				 sender.sendMessage(prefix +  ChatColor.RED + "Invalid usage: /coins add {username} {amount}");
			      sender.sendMessage( prefix + ChatColor.RED + "Invalid usage: /coins set {username} {amount}");
			      return false;
			    }
			 
			
			 if (args.length == 2)
			    {
				 sender.sendMessage(prefix +  ChatColor.RED + "Invalid usage: /coins add {username} {amount}");
			      sender.sendMessage( prefix + ChatColor.RED + "Invalid usage: /coins set {username} {amount}");	
			      return false;
			    }
			String arg0 = String.valueOf(args[0]);
			

			if(arg0.equalsIgnoreCase("add")){
				String arg1 = String.valueOf(args[1]);
				int arg2 = Integer.valueOf(args[2]);
	        	OfflinePlayer target1 = Bukkit.getOfflinePlayer(args[1]);

		        if(target1.isOnline() == false){
					String r = "ranksystem.coins." + target1.getName() ;
					int coins = Main.instance.getConfig().getInt(r);
					Main.instance.getConfig().set(r,  0);
					ConfigManager.onConfigOfflineCoins(target1, coins + arg2, "add");

		        }else {
			        Player target = Bukkit.getServer().getPlayer(args[1]);
					String r = "ranksystem.coins." + target.getName() ;
					int coinss = Main.instance.getConfig().getInt(r);

					Main.instance.getConfig().set(r,  0);
					
					ConfigManager.onConfigCoins(target, coinss + arg2, "add");
					int coins = Main.instance.getConfig().getInt(r);

					String message1 = prefix1 + ChatColor.GRAY + "The coins of " + DataManager.s(target) + ChatColor.GRAY + "" + " " + target.getName() + " has been set to " + coins + " coins!";
					String message = prefix1 + ChatColor.GRAY + "You received " + ChatColor.GOLD + "" + arg2 + ChatColor.GRAY + " coins!";
					ScoreboardManager123.updateScoreboardLobby(target);
					p.sendMessage(message1);
					target.sendMessage(message);

		        }
				
			}else if(arg0.equalsIgnoreCase("set")){
				
				String arg1 = String.valueOf(args[1]);
				int arg2 = Integer.valueOf(args[2]);
	        	OfflinePlayer target1 = Bukkit.getOfflinePlayer(args[1]);

		        if(target1.isOnline() == false){
					String r = "ranksystem.coins." + target1.getName() ;
					Main.instance.getConfig().set(r,  0);
					ConfigManager.onConfigOfflineCoins(target1, arg2, "set");

		        }else {
			        Player target = Bukkit.getServer().getPlayer(args[1]);
					String r = "ranksystem.coins." + target.getName() ;
					Main.instance.getConfig().set(r,  0);
					ConfigManager.onConfigCoins(target, arg2, "set");
					int coins = Main.instance.getConfig().getInt(r);

					String message1 = prefix1 + ChatColor.GRAY + "The coins of " + DataManager.s(target) + ChatColor.GRAY + "" + " " + target.getName() + " has been set to " + coins + " coins!";
					String message = prefix1 + ChatColor.GRAY + "Your coins have been set to " + ChatColor.GOLD + "" + arg2 + ChatColor.GRAY + " coins!";

					ScoreboardManager123.updateScoreboardLobby(target);
					p.sendMessage(message1);
					target.sendMessage(message);

		        }
			}
			}else{
				p.sendMessage(prefix + ChatColor.RED + "You dont have the permissions for this!");
				return false;
			}
		}else{
			 if (args.length == 0)
			    {
			      sender.sendMessage( prefix + ChatColor.RED + "Invalid usage: /coins add {username} {amount}");
			      sender.sendMessage( prefix + ChatColor.RED + "Invalid usage: /coins set {username} {amount}");
			      return false;
			    }
			 
			
			 if (args.length == 2)
			    {
			      sender.sendMessage(prefix +  ChatColor.RED + "Invalid usage: /coins add {username} {amount}");
			      sender.sendMessage( prefix + ChatColor.RED + "Invalid usage: /coins set {username} {amount}");

			      return false;
			    }
			String arg0 = String.valueOf(args[0]);
			String arg1 = String.valueOf(args[1]);
			int arg2 = Integer.valueOf(args[2]);
	        Player target = Bukkit.getServer().getPlayer(args[1]);

			if(arg0.equalsIgnoreCase("add")){

				OfflinePlayer target1 = Bukkit.getOfflinePlayer(args[1]);

		        if(target1.isOnline() == false){
					String r = "ranksystem.coins." + target.getName() ;
					int coins = Main.instance.getConfig().getInt(r);

		        	ConfigManager.onConfigOfflineCoins(target1, coins , "add");

		        }else {
					String r = "ranksystem.coins." + target.getName() ;

					int coins = Main.instance.getConfig().getInt(r);

					ConfigManager.onConfigCoins(target, coins + arg2, "add");

					int coinss = Main.instance.getConfig().getInt(r);

					String message = prefix1 + ChatColor.GRAY + "You received " + ChatColor.GOLD + "" + coinss + ChatColor.GRAY + " coins!";

					String message1 = prefix1 + ChatColor.GRAY + "The coins of " + DataManager.s(target) + ChatColor.GRAY + "" + " " + target.getName() + " has been set to " + coinss + " coins!";
					ScoreboardManager123.updateScoreboardLobby(target);
					sender.sendMessage(message1);
					target.sendMessage(message);

		        }
					ScoreboardManager123.updateScoreboardLobby(target);
			}else if(arg0.equalsIgnoreCase("set")){
				OfflinePlayer target1 = Bukkit.getOfflinePlayer(args[1]);

		        if(target1.isOnline() == false){

					ConfigManager.onConfigOfflineCoins(target, arg2, "set");

		        }else {

					ConfigManager.onConfigCoins(target, arg2, "set");
					String r = "ranksystem.coins." + target.getName() ;

					int coins = Main.instance.getConfig().getInt(r);

					String message = prefix1 + ChatColor.GRAY + "Your coins have been set to " + ChatColor.GOLD + "" + arg2 + ChatColor.GRAY + " coins!";

					String message1 = prefix1 + ChatColor.GRAY + "The coins of " + DataManager.s(target) + ChatColor.GRAY + "" + " " + target.getName() + " has been set to " + coins + " coins!";
					ScoreboardManager123.updateScoreboardLobby(target);
					sender.sendMessage(message1);
					target.sendMessage(message);

		        }
					ScoreboardManager123.updateScoreboardLobby(target);
			}
				

			}
		
		
		return false;
	}
		

}
