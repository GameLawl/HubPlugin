package com.system.ranksystem.manager;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.system.ranksystem.Main;

public class ConfigManager {

	public static void onConfig(Player p, String rank, String m){
		switch(m.toString().toUpperCase()){
		case("REMOVE"):
			break;
		case("ADD"):
			String r = "ranksystem.rank." + p.getName() ;
			Main.instance.getConfig().set(r, rank);
			
			Main.instance.saveConfig();
			Main.instance.reloadConfig();
			break;
		case("RELOAD"):
			break;
		
		}
	}
	
	public static void onConfigOffline(OfflinePlayer p, String rank, String m){
		switch(m.toString().toUpperCase()){
		case("REMOVE"):
			break;
		case("ADD"):
			String r = "ranksystem.rank." + p.getName() ;
			Main.instance.getConfig().set(r, rank);
			Main.instance.saveConfig();
			Main.instance.reloadConfig();
			break;
		case("RELOAD"):
			break;
		
		}
	}
	
	public static void onConfigCoins(Player p, int coins, String m){
		switch(m.toString().toUpperCase()){
		case("REMOVE"):
			break;
		case("SET"):
			String r = "ranksystem.coins." + p.getName() ;
		Main.instance.getConfig().set(r, coins);
			
			Main.instance.saveConfig();
			Main.instance.reloadConfig();
			break;
		case("ADD"):
			String r2 = "ranksystem.coins." + p.getName() ;
		Main.instance.getConfig().set(r2, coins);
			
			Main.instance.saveConfig();
			Main.instance.reloadConfig();
			break;
		case("RELOAD"):
			break;
		
		}
	}
	
	public static void onConfigOfflineCoins(OfflinePlayer p, int coins, String m){
		switch(m.toString().toUpperCase()){
		case("REMOVE"):
			break;
		case("SET"):
			String r = "ranksystem.coins." + p.getName() ;
		Main.instance.getConfig().set(r, coins);
			
			Main.instance.saveConfig();
			Main.instance.reloadConfig();
			break;
		case("ADD"):
			String r2 = "ranksystem.coins." + p.getName() ;
			Main.instance.getConfig().set(r2,  coins);
			Main.instance.saveConfig();
			Main.instance.reloadConfig();
			break;
		case("RELOAD"):
			break;
		
		}
	}
}
