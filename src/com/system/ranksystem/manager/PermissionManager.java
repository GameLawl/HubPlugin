package com.system.ranksystem.manager;

import java.util.Arrays;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.system.ranksystem.Main;

public class PermissionManager {

	private static boolean ret;

	
	public static void onMethod(Player p, String rank, String m){
		switch(m.toUpperCase()){
		case("ADD"):
			onAdd(p, rank);
			break;
		
		}
			
	}
	
	private static void onAdd(Player p, String rank){
		String r = "ranksystem.permissions." + p.getName() ;

		switch(rank.toUpperCase()){
		case("DEFAULT"):
			String[] list1 = {"default"};

		Main.instance.getConfig().set(r, Arrays.asList(list1));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("VIP"):
		String[] list2 = {"default, vip"};
		Main.instance.getConfig().set(r, Arrays.asList(list2));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("DONATOR"):
			String[] list3 = {"default, vip, donator"};
		Main.instance.getConfig().set(r, Arrays.asList(list3));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("JRSUPPORT"):
			String[] list4 = {"default, vip, donator, support"};
		Main.instance.getConfig().set(r, Arrays.asList(list4));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("SUPPORTTRAIL"):
			String[] list41 = {"default, vip, donator, support"};
			Main.instance.getConfig().set(r, Arrays.asList(list41));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("SUPPORT"):
			String[] list42 = {"default, vip, donator, support"};
			Main.instance.getConfig().set(r, Arrays.asList(list42));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("HEADSUPPORT"):
			String[] list43 = {"default, vip, donator, support"};
		Main.instance.getConfig().set(r, Arrays.asList(list43));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("JRMODERATOR"):
			String[] list5 = {"default, vip, donator, support, moderator"};
		Main.instance.getConfig().set(r, Arrays.asList(list5));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("MODERATORTRAIL"):
			String[] list51 = {"default, vip, donator, support, moderator"};
		Main.instance.getConfig().set(r, Arrays.asList(list51));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("MODERATOR"):
			String[] list52 = {"default, vip, donator, support, moderator"};
		Main.instance.getConfig().set(r, Arrays.asList(list52));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("HEADMODERATOR"):
			String[] list53 = {"default, vip, donator, support,  moderator"};
		Main.instance.getConfig().set(r, Arrays.asList(list53));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("JRBUILDTEAM"):
			String[] list6 = {"default, vip, donator, support, buildteam"};
		Main.instance.getConfig().set(r, Arrays.asList(list6));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("BUILDTEAMTRAIL"):
			String[] list61 = {"default, vip, donator, support, buildteam"};
		Main.instance.getConfig().set(r, Arrays.asList(list61));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("BUILDTEAM"):
			String[] list62 = {"default, vip, donator, support, buildteam"};
		Main.instance.getConfig().set(r, Arrays.asList(list62));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("BUILDTEAMLEADER"):
			String[] list63 = {"default, vip, donator, support, buildteam"};
		Main.instance.getConfig().set(r, Arrays.asList(list63));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("YOUTUBE"):
			String[] list7 = {"default, vip, donator"};
		Main.instance.getConfig().set(r, Arrays.asList(list7));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("JRADMIN"):
			String[] list8 = {"default, vip, donator, moderator, support, admin"};
		Main.instance.getConfig().set(r, Arrays.asList(list8));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("ADMINTRAIL"):
			String[] list81 = {"default, vip, donator, moderator, support, admin"};
		Main.instance.getConfig().set(r, Arrays.asList(list81));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("ADMIN"):
			String[] list82 = {"default, vip, donator, moderator, support, buildteam, admin"};
		Main.instance.getConfig().set(r, Arrays.asList(list82));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("HEADADMIN"):
			String[] list83 = {"default, vip, donator, moderator, support, buildteam, admin, adminhead"};
		Main.instance.getConfig().set(r, Arrays.asList(list83));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("JRDEV"):
			String[] list9 = {"default, vip, donator, moderator, support, buildteam, admin, dev"};
		Main.instance.getConfig().set(r, Arrays.asList(list9));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("DEVTRAIL"):
			String[] list91 = {"default, vip, donator, moderator, support, buildteam, admin, dev"};
		Main.instance.getConfig().set(r, Arrays.asList(list91));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("DEV"):
			String[] list92 = {"default, vip, donator, moderator, support, buildteam, admin, dev"};
		Main.instance.getConfig().set(r, Arrays.asList(list92));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
		
		
			break;

		case("HEADDEV"):
			String[] list233 = {"default, vip, donator, moderator, support, buildteam, admin, dev, headdev, adminhead"};
			Main.instance.getConfig().set(r, Arrays.asList(list233));
			Main.instance.saveConfig();
			Main.instance.reloadConfig();
			break;
		case("OWNER"):
			String[] list = {"default, vip, donator, moderator, support, buildteam, admin, dev, headdev, adminhead, owner"};
		Main.instance.getConfig().set(r, Arrays.asList(list));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
		}
	}
	
	public static void onMethodOffline(OfflinePlayer p, String rank, String m){
		switch(m.toUpperCase()){
		case("ADD"):
			onAdd1(p, rank);
			break;
		
		}
			
	}
	
	private static void onAdd1(OfflinePlayer p, String rank){
		String r = "ranksystem.permissions." + p.getName() ;

		switch(rank.toUpperCase()){
		case("DEFAULT"):
			String[] list1 = {"default"};

		Main.instance.getConfig().set(r, Arrays.asList(list1));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("VIP"):
		String[] list2 = {"default, vip"};
		Main.instance.getConfig().set(r, Arrays.asList(list2));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("DONATOR"):
			String[] list3 = {"default, vip, donator"};
		Main.instance.getConfig().set(r, Arrays.asList(list3));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("JRSUPPORT"):
			String[] list4 = {"default, vip, donator, support"};
		Main.instance.getConfig().set(r, Arrays.asList(list4));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("SUPPORTTRAIL"):
			String[] list41 = {"default, vip, donator, support"};
			Main.instance.getConfig().set(r, Arrays.asList(list41));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("SUPPORT"):
			String[] list42 = {"default, vip, donator, support"};
			Main.instance.getConfig().set(r, Arrays.asList(list42));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("HEADSUPPORT"):
			String[] list43 = {"default, vip, donator, support"};
		Main.instance.getConfig().set(r, Arrays.asList(list43));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("JRMODERATOR"):
			String[] list5 = {"default, vip, donator, support, moderator"};
		Main.instance.getConfig().set(r, Arrays.asList(list5));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("MODERATORTRAIL"):
			String[] list51 = {"default, vip, donator, support, moderator"};
		Main.instance.getConfig().set(r, Arrays.asList(list51));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("MODERATOR"):
			String[] list52 = {"default, vip, donator, support, moderator"};
		Main.instance.getConfig().set(r, Arrays.asList(list52));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("HEADMODERATOR"):
			String[] list53 = {"default, vip, donator, support,  moderator"};
		Main.instance.getConfig().set(r, Arrays.asList(list53));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("JRBUILDTEAM"):
			String[] list6 = {"default, vip, donator, support, buildteam"};
		Main.instance.getConfig().set(r, Arrays.asList(list6));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("BUILDTEAMTRAIL"):
			String[] list61 = {"default, vip, donator, support, buildteam"};
		Main.instance.getConfig().set(r, Arrays.asList(list61));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("BUILDTEAM"):
			String[] list62 = {"default, vip, donator, support, buildteam"};
		Main.instance.getConfig().set(r, Arrays.asList(list62));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("BUILDTEAMLEADER"):
			String[] list63 = {"default, vip, donator, support, buildteam"};
		Main.instance.getConfig().set(r, Arrays.asList(list63));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("YOUTUBE"):
			String[] list7 = {"default, vip, donator"};
		Main.instance.getConfig().set(r, Arrays.asList(list7));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("JRADMIN"):
			String[] list8 = {"default, vip, donator, moderator, support, buildteam, admin"};
		Main.instance.getConfig().set(r, Arrays.asList(list8));
	Main.instance.saveConfig();
	Main.instance.reloadConfig();
			break;
		case("ADMINTRAIL"):
			String[] list81 = {"default, vip, donator, moderator, support, buildteam, admin"};
		Main.instance.getConfig().set(r, Arrays.asList(list81));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("ADMIN"):
			String[] list82 = {"default, vip, donator, moderator, support, buildteam, admin"};
		Main.instance.getConfig().set(r, Arrays.asList(list82));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("HEADADMIN"):
			String[] list83 = {"default, vip, donator, moderator, support, buildteam, admin, adminhead"};
		Main.instance.getConfig().set(r, Arrays.asList(list83));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("JRDEV"):
			String[] list9 = {"default, vip, donator, moderator, support, buildteam, admin, dev"};
		Main.instance.getConfig().set(r, Arrays.asList(list9));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("DEVTRAIL"):
			String[] list91 = {"default, vip, donator, moderator, support, buildteam, admin, dev"};
		Main.instance.getConfig().set(r, Arrays.asList(list91));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("DEV"):
			String[] list92 = {"default, vip, donator, moderator, support, buildteam, admin, dev"};
		Main.instance.getConfig().set(r, Arrays.asList(list92));
		Main.instance.saveConfig();
		Main.instance.reloadConfig();
			break;
		case("HEADDEV"):
			String[] list233 = {"default, vip, donator, moderator, support, buildteam, admin, dev, headdev, adminhead"};
			Main.instance.getConfig().set(r, Arrays.asList(list233));
			Main.instance.saveConfig();
			Main.instance.reloadConfig();
			break;
		case("OWNER"):
			String[] list = {"default, vip, donator, moderator, support, buildteam, admin, dev, headdev, adminhead, owner"};
			Main.instance.getConfig().set(r, Arrays.asList(list));
			Main.instance.saveConfig();
			Main.instance.reloadConfig();
			break;
		case("PIGGY"):
			String[] list232323 = {"default, vip"};
			Main.instance.getConfig().set(r, Arrays.asList(list232323));
			Main.instance.saveConfig();
			Main.instance.reloadConfig();
			break;
		}
	}
	public static boolean hasPermission(Player p, String permission){
		String r = "ranksystem.permissions." + p.getName() ;
		String r1 = Main.instance.getConfig().getString(r).replace("[", "");
		String r2 = r1.replace(",", "");
		String r3 = r2.replace("]", "");

		if(r3.contains(permission)){
			ret = true;

		}else if(!(r3.contains(permission))){
			ret = false;

		}
		return ret;
		}
}
