package com.system.ranksystem.manager;

import org.bukkit.entity.Player;

import com.system.ranksystem.Main;

public class AutoCoinManager {

	public static void onCheck(Player p){
		if(DataManager.coins.containsKey(p.getName())){
			System.out.println("Data Checked for Player " + p.getName() + " everything is up to date now!");

		}else{
			String r = "ranksystem.coins." + p.getName() ;
			Main.instance.getConfig().addDefault(r, "0");
			Main.instance.getConfig().options().copyDefaults(false);

		int rank = Main.instance.getConfig().getInt(r);
		System.out.println(rank);

				ConfigManager.onConfigCoins(p, rank, "add");
				System.out.println("Data Checked for Player " + p.getName() + " everything is up to date now!");
		}
	}
}
