package com.system.ranksystem.manager;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class DataManager {
	public static HashMap<String, Integer> coins = new HashMap<String, Integer>();

	public static HashMap<String, String> gadget = new HashMap<String, String>();

	public static HashMap<String, String> rank = new HashMap<String, String>();
	public static HashMap<String, String> prefix = new HashMap<String, String>();
	public static HashSet<String> flying = new HashSet<String>();
	public static HashSet<String> tptoggled = new HashSet<String>();
	public static HashMap<String, Inventory> pinvstatics = new HashMap<String, Inventory>();
	public static HashMap<String, Inventory> plobbyinv = new HashMap<String, Inventory>();
	public static HashMap<String, Inventory> pteleporterinv = new HashMap<String, Inventory>();
	public static HashMap<String, Inventory> pshopinv = new HashMap<String, Inventory>();
	public static HashMap<String, Inventory> pgadgetinv = new HashMap<String, Inventory>();
	public static HashMap<String, Inventory> pdisguiseinv = new HashMap<String, Inventory>();

	
	
	
	public static HashMap<String, String[]> list = new HashMap<String, String[]>();
	public static String s(Player p){
		String prefix1 = prefix.get(p.getName());
		String prefix2 = prefix1.replace("{", "");
		String prefix3 = prefix2.replace(p.getName(), "");
		String prefix4 = prefix3.replace("=", "");
		String prefix5 = prefix4.replace("}", "");
		return prefix5;
	}
	
	public static List<String> list(){
		String[] prefix1 = list.get("test");
	
		return Arrays.asList(prefix1);
	}
	
	public static String prefix(String m, Player p, String rank){
		String full = null;
		switch(m.toUpperCase()){
		case("ADD"):
			String prefix = ChatColor.YELLOW + "RANK>>";
			String message = ChatColor.GRAY + " You have been given the rank " + DataManager.prefix.get(p.getName()) + "!";
			full = prefix + message;
			break;
		case("REMOVE"):
			String prefix1 = ChatColor.YELLOW + "RANK>>";

		String message2 = ChatColor.GRAY + " You have been removed from the rank " + DataManager.prefix.get(p.getName()) + "!";
		full = prefix1 + message2;
			break;
		
		}
		
		return full;

		
	}
	
	public static void onTodo(Player p){
		String line1 = "====================================================";
		String line2 = "                    BASIC                           ";
		String line3 = "====================================================";
		String todo1 = ChatColor.STRIKETHROUGH + "- Ranks";
		String todo2 = ChatColor.STRIKETHROUGH + "- BanSystem";
		String todo3 = ChatColor.ITALIC + "- Gui. [For Shop, Lobbies, Games, Gadgets]";
		String todo4 = "- Effects.";
		String todo5 = "- Pets.";
		String todo6 = ChatColor.ITALIC + "- Custom Commands. [Fly, Gamemode, etc.]";
		String todo7 = "- AnitHackSystem. [FlyHack, AntiRapidBowHack, etc.]";
		String todo8 = "- Login Check IP.";
		String todo9 = "- Coins.";
		String todo10 = "- Parties";
		String todo11 = ChatColor.ITALIC + "- Scoreboard. [Player Data]";
		String todo17 = "- Bungeecord cross server commands";
		String line4 = "====================================================";
		String line5 = "                    ADVANCED                        ";
		String line6 = "====================================================";
		String todo12 = ChatColor.ITALIC + "- Database";
		String todo13 = "- More Lobbies";
		String todo14 = "- Firework Timer, when done play firework around spawn.";
		String todo15 = "- Statics.";
		String todo16 = "- Achievements.";

		String line7 = "====================================================";

		p.sendMessage(line1);
		p.sendMessage(line2);
		p.sendMessage(line3);
		p.sendMessage(todo1);
		p.sendMessage(todo2);
		p.sendMessage(todo3);
		p.sendMessage(todo4);
		p.sendMessage(todo5);
		p.sendMessage(todo6);
		p.sendMessage(todo7);
		p.sendMessage(todo8);
		p.sendMessage(todo9);
		p.sendMessage(todo10);
		p.sendMessage(todo11);
		p.sendMessage(todo17);

		p.sendMessage(line4);
		p.sendMessage(line5);
		p.sendMessage(line6);
		p.sendMessage(todo12);
		p.sendMessage(todo13);
		p.sendMessage(todo14);
		p.sendMessage(todo15);
		p.sendMessage(todo16);

		p.sendMessage(line7);


	}
	
}
