package com.system.ranksystem.manager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.system.hubsystem.manager.ScoreboardManager123;

public class CommandManager implements CommandExecutor {


	public static String prefix = ChatColor.GREEN + "COMMAND>> ";
	public static String prefix1 = ChatColor.YELLOW + "RANK>> ";	

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		// TODO Auto-generated method stub
			if(sender instanceof Player){
				if(cmd.getName().equalsIgnoreCase("rank")){
					Player p = (Player) sender;
					if(PermissionManager.hasPermission(p, "admin") == true){
						 if (args.length == 0)
						    {
						      sender.sendMessage(prefix + ChatColor.RED + "Invalid usage: /rank add {username} {rank}");
						      return true;
						    }
						 
						
						 if (args.length == 2)
						    {
						      sender.sendMessage(prefix + ChatColor.RED + "Invalid usage: /rank add {username} {rank}");
						      return true;
						    }
					String arg0 = String.valueOf(args[0]);
					

					if(arg0.equalsIgnoreCase("add")){
						String arg1 = String.valueOf(args[1]);
						String arg2 = String.valueOf(args[2]);
			        	OfflinePlayer target1 = Bukkit.getOfflinePlayer(args[1]);

				        if(target1.isOnline() == false){

							ConfigManager.onConfigOffline(target1, arg2, arg0);

				        }else {
					        Player target = Bukkit.getServer().getPlayer(args[1]);

							RankManager.onMethod(target, arg2, arg1.toLowerCase());
							PrefixManager.onMethod(target, arg2);
							ConfigManager.onConfig(target, arg2, arg0);
							PermissionManager.onMethod(target, arg2, arg0);
							target.sendMessage(DataManager.prefix("add", target, arg2));
							
							String message = ChatColor.GRAY + target.getName() + " has been given the rank " + DataManager.prefix.get(target.getName()) + "!";
							String full = prefix1 + message;
							sender.sendMessage(full);
							ScoreboardManager123.updateScoreboardLobby(target);

				        }
						
					}else if(arg0.equalsIgnoreCase("list")){
						p.sendMessage("" + DataManager.list());
					}
					}else{
						p.sendMessage(prefix + ChatColor.RED + "You dont have the permissions for this!");
						return false;
					
					
					}
					}
					
			}else if(!(sender instanceof Player)){
				 if (args.length == 0)
				    {
				      sender.sendMessage(prefix + ChatColor.RED + "Invalid usage: /rank add {username} {rank}");
				      return true;
				    }
				 
				
				 if (args.length == 2)
				    {
				      sender.sendMessage(prefix + ChatColor.RED + "Invalid usage: /rank add {username} {rank}");
				      return true;
				    }
				String arg0 = String.valueOf(args[0]);
				String arg1 = String.valueOf(args[1]);
				String arg2 = String.valueOf(args[2]);
		        Player target = Bukkit.getServer().getPlayer(args[1]);

				if(arg0.equalsIgnoreCase("add")){
					OfflinePlayer target1 = Bukkit.getOfflinePlayer(args[1]);

			        if(target1.isOnline() == false){

						ConfigManager.onConfigOffline(target1, arg2, arg0);

			        }else {

						RankManager.onMethod(target, arg2, arg1.toLowerCase());
						PrefixManager.onMethod(target, arg2);
						ConfigManager.onConfig(target, arg2, arg0);
						PermissionManager.onMethod(target, arg2, arg0);
						target.sendMessage(DataManager.prefix("add", target, arg2));
						String message = ChatColor.GRAY + target.getName() + " has been given the rank " + DataManager.prefix.get(target.getName()) + "!";
						String full = prefix1 + message;
						sender.sendMessage(full);
						ScoreboardManager123.updateScoreboardLobby(target);
			        }
					

				}
			}
			

		
		

			
			return false;
	}
}


