package com.system.ranksystem.manager;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class PrefixManager {
	
	public static void onMethod(Player p, String rank){
		DataManager.prefix.remove(p.getName());
		switch(rank.toString().toUpperCase()){
		case("DEFAULT"):
			DataManager.prefix.put(p.getName(), ChatColor.GRAY + "DEFAULT");
			break;
		case("VIP"):
			DataManager.prefix.put(p.getName(), ChatColor.DARK_AQUA + "VIP");

			break;
		case("DONATOR"):
			DataManager.prefix.put(p.getName(), ChatColor.GOLD + "DONATOR");
			break;
		case("JRSUPPORT"):
			DataManager.prefix.put(p.getName(), ChatColor.YELLOW + "JRSUPPORT");
			break;
		case("PIGGY"):
			DataManager.prefix.put(p.getName(), ChatColor.LIGHT_PURPLE + "PIGGY");

			break;
		case("SUPPORTTRAIL"):
			DataManager.prefix.put(p.getName(), ChatColor.YELLOW + "" + ChatColor.ITALIC + "SUPPORT");
			break;
		case("SUPPORT"):
			DataManager.prefix.put(p.getName(), ChatColor.YELLOW + "SUPPORT");
			break;
		case("HEADSUPPORT"):
			DataManager.prefix.put(p.getName(), ChatColor.YELLOW + ""  + ChatColor.BOLD +  "SUPPORT" + ChatColor.GOLD + "+");
			break;
		case("JRMODERATOR"):
			DataManager.prefix.put(p.getName(), ChatColor.GREEN + "JRMODERATOR");
			break;
		case("MODERATORTRAIL"):
			DataManager.prefix.put(p.getName(), ChatColor.GREEN + "" + ChatColor.ITALIC +  "MODERATOR");
			break;
		case("MODERATOR"):
			DataManager.prefix.put(p.getName(), ChatColor.GREEN + "MODERATOR");
			break;
		case("HEADMODERATOR"):
			DataManager.prefix.put(p.getName(), ChatColor.GREEN + "" + ChatColor.BOLD +  "MODERATOR" + ChatColor.GOLD + "+");
			break;
		case("JRBUILDTEAM"):
			DataManager.prefix.put(p.getName(), ChatColor.AQUA + "JRBUILDTEAM");
			break;
		case("BUILDTEAMTRAIL"):
			DataManager.prefix.put(p.getName(), ChatColor.AQUA + "" + ChatColor.ITALIC + "BUILDTEAM");
			break;
		case("BUILDTEAM"):
			DataManager.prefix.put(p.getName(), ChatColor.AQUA + "BUILDTEAM");
			break;
		case("BUILDTEAMLEADER"):
			DataManager.prefix.put(p.getName(), ChatColor.AQUA + "" +  ChatColor.BOLD +  "BUILDTEAM" + ChatColor.GOLD + "+");
			break;
		case("YOUTUBE"):
			DataManager.prefix.put(p.getName(), ChatColor.RED + "" + ChatColor.BOLD + "YOU" + ChatColor.WHITE + "" + ChatColor.BOLD + "TUBE");
			break;
		case("JRADMIN"):
			DataManager.prefix.put(p.getName(), ChatColor.DARK_RED + ""  + "JRADMIN");
			break;
		case("ADMINTRAIL"):
			DataManager.prefix.put(p.getName(), ChatColor.DARK_RED + "" + "" + ChatColor.ITALIC + "ADMIN");
			break;
		case("ADMIN"):
			DataManager.prefix.put(p.getName(), ChatColor.DARK_RED + "" + "ADMIN");
			break;
		case("HEADADMIN"):
			DataManager.prefix.put(p.getName(), ChatColor.DARK_RED + "" + ChatColor.BOLD + "" + "ADMIN");
			break;
		case("JRDEV"):
			DataManager.prefix.put(p.getName(), ChatColor.RED + "" + "JRDEV");
			break;
		case("SPONSOR"):
			DataManager.prefix.put(p.getName(), ChatColor.DARK_GREEN + "SPONSOR");
			break;
		case("DEVTRAIL"):
			DataManager.prefix.put(p.getName(), ChatColor.RED + "" + ChatColor.ITALIC + "DEV");
			break;
		case("DEV"):
			DataManager.prefix.put(p.getName(), ChatColor.RED + "" + "DEV");
			break;
		case("HEADDEV"):
			DataManager.prefix.put(p.getName(), ChatColor.RED + "" + ChatColor.BOLD + "" + "DEV" + ChatColor.GOLD + "+");
			break;
		case("OWNER"):
			DataManager.prefix.put(p.getName(), ChatColor.RED + ""  + ChatColor.BOLD + "" + "OWNER");
			break;
			
		

		}
	}
	
	public static void onMethodOffline(OfflinePlayer p, String rank){
		DataManager.prefix.remove(p.getName());
		switch(rank.toString().toUpperCase()){
		case("DEFAULT"):
			DataManager.prefix.put(p.getName(), ChatColor.GRAY + "");
			break;
		case("VIP"):
			DataManager.prefix.put(p.getName(), ChatColor.DARK_AQUA + "VIP");

			break;
		case("DONATOR"):
			DataManager.prefix.put(p.getName(), ChatColor.GOLD + "DONATOR");
			break;
		case("JRSUPPORT"):
			DataManager.prefix.put(p.getName(), ChatColor.YELLOW + "JRSUPPORT");
			break;
		case("SUPPORTTRAIL"):
			DataManager.prefix.put(p.getName(), ChatColor.YELLOW + "" + ChatColor.ITALIC + "SUPPORT");
			break;
		case("SUPPORT"):
			DataManager.prefix.put(p.getName(), ChatColor.YELLOW + "SUPPORT");
			break;
		case("HEADSUPPORT"):
			DataManager.prefix.put(p.getName(), ChatColor.YELLOW + ""  + ChatColor.BOLD +  "SUPPORT" + ChatColor.GOLD + "+");
			break;
		case("JRMODERATOR"):
			DataManager.prefix.put(p.getName(), ChatColor.GREEN + "JRMODERATOR");
			break;
		case("MODERATORTRAIL"):
			DataManager.prefix.put(p.getName(), ChatColor.GREEN + "" + ChatColor.ITALIC +  "MODERATOR");
			break;
		case("MODERATOR"):
			DataManager.prefix.put(p.getName(), ChatColor.GREEN + "MODERATOR");
			break;
		case("HEADMODERATOR"):
			DataManager.prefix.put(p.getName(), ChatColor.GREEN + "" + ChatColor.BOLD +  "MODERATOR" + ChatColor.GOLD + "+");
			break;
		case("JRBUILDTEAM"):
			DataManager.prefix.put(p.getName(), ChatColor.AQUA + "JRBUILDTEAM");
			break;
		case("BUILDTEAMTRAIL"):
			DataManager.prefix.put(p.getName(), ChatColor.AQUA + "" + ChatColor.ITALIC + "BUILDTEAM");
			break;
		case("BUILDTEAM"):
			DataManager.prefix.put(p.getName(), ChatColor.AQUA + "BUILDTEAM");
			break;
		case("BUILDTEAMLEADER"):
			DataManager.prefix.put(p.getName(), ChatColor.AQUA + "" +  ChatColor.BOLD +  "BUILDTEAM" + ChatColor.GOLD + "+");
			break;
		case("YOUTUBE"):
			DataManager.prefix.put(p.getName(), ChatColor.RED + "" + ChatColor.BOLD + "YOU" + ChatColor.WHITE + "" + ChatColor.BOLD + "TUBE");
			break;
		case("JRADMIN"):
			DataManager.prefix.put(p.getName(), ChatColor.DARK_RED + ""  + "JRADMIN");
			break;
		case("ADMINTRAIL"):
			DataManager.prefix.put(p.getName(), ChatColor.DARK_RED + "" + "" + ChatColor.ITALIC + "ADMIN");
			break;
		case("ADMIN"):
			DataManager.prefix.put(p.getName(), ChatColor.DARK_RED + "" + "ADMIN");
			break;
		case("PIGGY"):
			DataManager.prefix.put(p.getName(), ChatColor.LIGHT_PURPLE + "PIGGY");

			break;
		case("HEADADMIN"):
			DataManager.prefix.put(p.getName(), ChatColor.DARK_RED + "" + ChatColor.BOLD + "" + "ADMIN");
			break;
		case("JRDEV"):
			DataManager.prefix.put(p.getName(), ChatColor.RED + "" + "JRDEV");

			break;
		case("DEVTRAIL"):
			DataManager.prefix.put(p.getName(), ChatColor.RED + "" + ChatColor.ITALIC + "DEV");
			break;
		case("SPONSOR"):
			DataManager.prefix.put(p.getName(), ChatColor.DARK_GREEN + "SPONSOR");
			break;
		case("DEV"):
			DataManager.prefix.put(p.getName(), ChatColor.RED + "" + "DEV");
			break;
		case("HEADDEV"):
			DataManager.prefix.put(p.getName(), ChatColor.RED + "" + ChatColor.BOLD + "" + "DEV" + ChatColor.GOLD + "+");
			break;
		case("OWNER"):
			DataManager.prefix.put(p.getName(), ChatColor.RED + "" +  ChatColor.BOLD + "" + "OWNER");
			break;
		
		

		}
	}

}
