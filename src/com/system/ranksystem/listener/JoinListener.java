package com.system.ranksystem.listener;

import java.io.File;
import java.util.Calendar;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import com.system.bungeecord.manager.BungeecordManager;
import com.system.commandsystem.executors.Vanish;
import com.system.gadgetssystem.pets.PetManager;
import com.system.hubsystem.manager.BarManager;
import com.system.hubsystem.manager.ItemManager;
import com.system.hubsystem.manager.ScoreboardManager123;
import com.system.inventorysystem.inventories.GadgetInventory;
import com.system.inventorysystem.inventories.LobbyInventory;
import com.system.inventorysystem.inventories.ShopInventory;
import com.system.inventorysystem.inventories.StaticsInventory;
import com.system.inventorysystem.inventories.TeleportInventory;
import com.system.ranksystem.Main;
import com.system.ranksystem.manager.AutoCoinManager;
import com.system.ranksystem.manager.AutoRankManager;
import com.system.ranksystem.manager.ConfigManager;
import com.system.ranksystem.manager.DataManager;
import com.system.ranksystem.manager.PermissionManager;
import com.system.ranksystem.manager.PrefixManager;
import com.system.ranksystem.manager.RankManager;

public class JoinListener implements Listener{
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onJoin(PlayerJoinEvent event){
		Player p = event.getPlayer();
		////////////////////
		
		
		p.setFoodLevel(20);
		p.getInventory().clear();
		p.getInventory().setItem(0, ItemManager.teleporter());
		p.getInventory().setItem(1, ItemManager.lobby());
		p.getInventory().setItem(8, ItemManager.shop());
		p.getInventory().setItem(4, ItemManager.statics());
		p.getInventory().setItem(7, ItemManager.gadgets());
		//BarManager.showBarChanging(p);

		p.sendMessage(ChatColor.GOLD + "** " + ChatColor.GRAY + "This plugin is made by GameLawl and ddgisvet" +  ChatColor.GOLD + " **" );

		p.updateInventory();
		AutoRankManager.onCheck(p);
		AutoCoinManager.onCheck(p);

		for (Player on : Bukkit.getOnlinePlayers()) {
			if(Vanish.vanished.contains(on.getName())){
				Player p1 = (Player) on;
				on.hidePlayer(p1);

			}
		}
		if(DataManager.prefix.containsKey(p.getName())){
		if(PermissionManager.hasPermission(p, "admin")){
			DataManager.onTodo(p);
			//PetManager.onCreatePet(p, "pig");
		}else{
		}
		}
		
			String r = "ranksystem.rank." + p.getName() ;
			Main.instance.getConfig().addDefault(r, "default");
			Main.instance.getConfig().options().copyDefaults(false);

		String rank = Main.instance.getConfig().getString(r);
		System.out.println(rank);
		String c = "ranksystem.coins." + p.getName() ;

		int coins = Main.instance.getConfig().getInt(c);
			RankManager.onMethod(p, rank, "add");
				PrefixManager.onMethod(p, rank);
				PermissionManager.onMethod(p, rank, "add");
				p.setCustomNameVisible(true);
				StaticsInventory.onMethod(p, "create");
				TeleportInventory.onMethod(p, "create");
				ShopInventory.onMethod(p, "create");		
				GadgetInventory.onMethod(p, "create");		
				LobbyInventory.onMethod(p, "create");		
				ConfigManager.onConfigCoins(p, coins, "add");

				if(PermissionManager.hasPermission(p, "vip") == true){
					event.setJoinMessage("" + DataManager.s(p) + "" + ChatColor.DARK_GRAY + "" + ChatColor.GRAY + " " + p.getName() + " has joined the game!");
					}else{
						event.setJoinMessage("");
					}				Main.instance.saveConfig();
				
				ScoreboardManager123.giveScoreboardLobby(p);
				
		}
		

		
		 
	
	
	 @EventHandler
	  public void onPlayerLogin(PlayerLoginEvent event){
	
	 
	  {
	    Player player = event.getPlayer();

	    if (player.isBanned())
	    {
	      FileConfiguration config = null;
	      File file = new File("plugins" + File.separator + "bans" + File.separator + "players" + File.separator + player.getName() + ".yml");
	      config = YamlConfiguration.loadConfiguration(file);
	      String banreason = config.getString("ban_reason");
	      event.disallow(PlayerLoginEvent.Result.KICK_BANNED, ChatColor.RED + "You are banned for " + banreason);
	    }
	  }
	 }
	


	public static void onDateBroadcast(Player p){
		Calendar cal = Calendar.getInstance();
		int dayOfMonth = cal.get(Calendar.DATE);
		String dayOfMonthStr = String.valueOf(dayOfMonth);
		int Year = cal.get(Calendar.YEAR);
		String YearStr = String.valueOf(Year);
		int Month = cal.get(Calendar.MONTH) + 1;
		String MonthStr = String.valueOf(Month);
		String date = dayOfMonthStr + "/" + MonthStr + "/" + YearStr;
		p.sendMessage( ChatColor.BLUE + "The date is " + "" + date);
	}
	
	public static void onTimeBroadcast(Player p){
		Calendar cal = Calendar.getInstance();
		 int Hour = cal.get(Calendar.HOUR_OF_DAY);
		 String HourStr = String.valueOf(Hour);
		 int Minute = cal.get(Calendar.MINUTE);
		 String MinuteStr = String.valueOf(Minute);
		 int sec = cal.get(Calendar.SECOND);
		 String Seconds = String.valueOf(sec);
		 int milisec = cal.get(Calendar.MILLISECOND);
		 String Milisec = String.valueOf(milisec);
		String time = HourStr + ":" + MinuteStr + ":" + Seconds + ";" + Milisec;
	
		p.sendMessage( ChatColor.BLUE + "The time is " + "" + time);
	}
	}


