package com.system.ranksystem.listener;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.system.ranksystem.manager.DataManager;
import com.system.ranksystem.manager.PermissionManager;

public class QuitListener implements Listener{
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event){
		Player p = event.getPlayer();
		if(PermissionManager.hasPermission(p, "vip") == true){
			event.setQuitMessage("" + DataManager.s(p) + "" + ChatColor.DARK_GRAY + "" + ChatColor.GRAY + " " + p.getName() + " has left the game!");
			}else{
				event.setQuitMessage("");
			}
	}

}
