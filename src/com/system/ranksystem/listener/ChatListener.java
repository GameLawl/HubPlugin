package com.system.ranksystem.listener;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.system.commandsystem.executors.Mute;
import com.system.commandsystem.executors.Silence;
import com.system.ranksystem.manager.DataManager;
import com.system.ranksystem.manager.PermissionManager;

public class ChatListener implements Listener{

	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		Player p = event.getPlayer();
		
		World w = p.getWorld();
		String world = w.toString();
		
		
		if ((Silence.chat) == true) {
			if(PermissionManager.hasPermission(p, "support") == true){
				if(p.getName().equals("GameLawl") || p.getName().equals("ddgisvet")){
					if(DataManager.prefix.get(p.getName()).contains("DEFAULT")){
						event.setFormat("" + ChatColor.GRAY + " " + p.getName() + ChatColor.RED + "" + ChatColor.BOLD + " DEV" + ChatColor.GOLD + "+ " +  ChatColor.DARK_GRAY +  " >> " + ChatColor.GRAY + "" + event.getMessage());

					}else{
						event.setFormat("" + DataManager.s(p) + "" + ChatColor.GRAY + " " + p.getName() + ChatColor.RED + "" + ChatColor.BOLD + " DEV" + ChatColor.GOLD + "+ " +  ChatColor.DARK_GRAY +  " >> " + ChatColor.GRAY + "" + event.getMessage());

					}

				}else{
					if(DataManager.prefix.get(p.getName()).contains("DEFAULT")){
						event.setFormat(""  + ChatColor.GRAY + " " + p.getName() + ChatColor.DARK_GRAY +  " >> " + ChatColor.GRAY + "" + event.getMessage());

					}else{
						event.setFormat("" + DataManager.s(p) + "" + ChatColor.GRAY + " " + p.getName() + ChatColor.DARK_GRAY +  " >> " + ChatColor.GRAY + "" + event.getMessage());

					}
					
					}
			}else{
				
				event.setCancelled(true);
				p.sendMessage(ChatColor.RED + "The chat is disabled!");
			//
				if(PermissionManager.hasPermission(p, "vip") == true){
					p.sendMessage(ChatColor.LIGHT_PURPLE + "You are a piggy!");
				}else{
					
				}
			}
			
		}else{
			if(p.getName().equals("GameLawl") || p.getName().equals("ddgisvet")){
				if(DataManager.prefix.get(p.getName()).contains("DEFAULT")){
					event.setFormat("" + ChatColor.GRAY + " " + p.getName() + ChatColor.RED + "" + ChatColor.BOLD + " DEV" + ChatColor.GOLD + "+ " +  ChatColor.DARK_GRAY +  " >> " + ChatColor.GRAY + "" + event.getMessage());

				}else{
					event.setFormat("" + DataManager.s(p) + "" + ChatColor.GRAY + " " + p.getName() + ChatColor.RED + "" + ChatColor.BOLD + " DEV" + ChatColor.GOLD + "+ " +  ChatColor.DARK_GRAY +  " >> " + ChatColor.GRAY + "" + event.getMessage());

				}
			}else{
				if(DataManager.prefix.get(p.getName()) == ChatColor.GRAY + "DEFAULT"){
					event.setFormat(""  + ChatColor.GRAY + " " + p.getName() + ChatColor.DARK_GRAY +  " >> " + ChatColor.GRAY + "" + event.getMessage());

				}else{
					event.setFormat("" + DataManager.s(p) + "" + ChatColor.GRAY + " " + p.getName() + ChatColor.DARK_GRAY +  " >> " + ChatColor.GRAY + "" + event.getMessage());

				}
				}
		}
		if(Mute.mute.contains(p.getName())) {
			if(PermissionManager.hasPermission(p, "support") == true){
				if(p.getName().equals("GameLawl") || p.getName().equals("ddgisvet")){
					if(DataManager.prefix.get(p.getName()).contains("DEFAULT")){
						event.setFormat("" + ChatColor.GRAY + " " + p.getName() + ChatColor.RED + "" + ChatColor.BOLD + " DEV" + ChatColor.GOLD + "+ " +  ChatColor.DARK_GRAY +  " >> " + ChatColor.GRAY + "" + event.getMessage());

					}else{
						event.setFormat("" + DataManager.s(p) + "" + ChatColor.GRAY + " " + p.getName() + ChatColor.RED + "" + ChatColor.BOLD + " DEV" + ChatColor.GOLD + "+ " +  ChatColor.DARK_GRAY +  " >> " + ChatColor.GRAY + "" + event.getMessage());

					}
				}else{
					if(DataManager.prefix.get(p.getName()) == ChatColor.GRAY + "DEFAULT"){
						event.setFormat(""  + ChatColor.GRAY + " " + p.getName() + ChatColor.DARK_GRAY +  " >> " + ChatColor.GRAY + "" + event.getMessage());

					}else{
						event.setFormat("" + DataManager.s(p) + "" + ChatColor.GRAY + " " + p.getName() + ChatColor.DARK_GRAY +  " >> " + ChatColor.GRAY + "" + event.getMessage());

					}				}
			}else{
				event.setCancelled(true);
				p.sendMessage(ChatColor.RED + "You are muted. You may not speak.");	
			}
		}else{
			if(p.getName().equals("GameLawl") || p.getName().equals("ddgisvet")){
				if(DataManager.prefix.get(p.getName()).contains("DEFAULT")){
					event.setFormat("" + ChatColor.GRAY + " " + p.getName() + ChatColor.RED + "" + ChatColor.BOLD + " DEV" + ChatColor.GOLD + "+ " +  ChatColor.DARK_GRAY +  " >> " + ChatColor.GRAY + "" + event.getMessage());

				}else{
					event.setFormat("" + DataManager.s(p) + "" + ChatColor.GRAY + " " + p.getName() + ChatColor.RED + "" + ChatColor.BOLD + " DEV" + ChatColor.GOLD + "+ " +  ChatColor.DARK_GRAY +  " >> " + ChatColor.GRAY + "" + event.getMessage());

				}
			}else{
				if(DataManager.prefix.get(p.getName()).contains("DEFAULT")){
					event.setFormat(""  + ChatColor.GRAY + " " + p.getName() + ChatColor.DARK_GRAY +  " >> " + ChatColor.GRAY + "" + event.getMessage());

				}else{
					event.setFormat("" + DataManager.s(p) + "" + ChatColor.GRAY + " " + p.getName() + ChatColor.DARK_GRAY +  " >> " + ChatColor.GRAY + "" + event.getMessage());

				}			}
		}
		}


	}


