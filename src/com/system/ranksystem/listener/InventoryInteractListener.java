package com.system.ranksystem.listener;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.system.bungeecord.manager.BungeecordManager;
import com.system.gadgetssystem.gadgets.GadgetManager;
import com.system.hubsystem.manager.ItemManager;
import com.system.ranksystem.manager.DataManager;

public class InventoryInteractListener implements Listener {
	public static String prefix1 = ChatColor.BLUE + "HUB>> ";
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event){
		Player p = (Player) event.getWhoClicked();
		if(event.getInventory().equals(DataManager.plobbyinv.get(p.getName()))){
			if(event.getCurrentItem().equals(ItemManager.LobbyItem())){
				if(ItemManager.LobbyItem().getType() == Material.REDSTONE_BLOCK){
			        p.getWorld().playSound(p.getLocation(), Sound.ZOMBIE_REMEDY, 3.0F, 3.0F);
					p.sendMessage(prefix1 + ChatColor.RED + "Sorry, but this server isn't online at the moment!");
					p.closeInventory();
					event.setCancelled(true);
				}else if(ItemManager.LobbyItem().getType() == Material.EMERALD_BLOCK){
					p.getWorld().playSound(p.getLocation(), Sound.LEVEL_UP, 10F, -5F);
					p.sendMessage(prefix1 + ChatColor.GRAY + "Sending to Lobby 1!");
					p.closeInventory();
					event.setCancelled(true);
				}
			}else if(event.getCurrentItem().equals(ItemManager.LobbyItem1())){
				if(ItemManager.LobbyItem1().getType() == Material.REDSTONE_BLOCK){
			        p.getWorld().playSound(p.getLocation(), Sound.ZOMBIE_REMEDY, 3.0F, 3.0F);
					p.sendMessage(prefix1 + ChatColor.RED + "Sorry, but this server isn't online at the moment!");
					p.closeInventory();
					event.setCancelled(true);
				}else if(ItemManager.LobbyItem1().getType() == Material.EMERALD_BLOCK){
					p.getWorld().playSound(p.getLocation(), Sound.LEVEL_UP, 10F, -5F);
					p.sendMessage(prefix1 + ChatColor.GRAY + "Sending to Lobby 2!");
					p.closeInventory();
					event.setCancelled(true);
				}
			}else if(event.getCurrentItem().equals(ItemManager.LobbyItem2())){
				if(ItemManager.LobbyItem2().getType() == Material.REDSTONE_BLOCK){
			        p.getWorld().playSound(p.getLocation(), Sound.ZOMBIE_REMEDY, 3.0F, 3.0F);
					p.sendMessage(prefix1 + ChatColor.RED + "Sorry, but this server isn't online at the moment!");
					p.closeInventory();
					event.setCancelled(true);
				}else if(ItemManager.LobbyItem2().getType() == Material.EMERALD_BLOCK){
					p.getWorld().playSound(p.getLocation(), Sound.LEVEL_UP, 10F, -5F);
					p.sendMessage(prefix1 + ChatColor.GRAY + "Sending to Lobby 3!");
					p.closeInventory();
					event.setCancelled(true);
				}
			}else if(event.getCurrentItem().equals(ItemManager.LobbyItem3())){
				if(ItemManager.LobbyItem3().getType() == Material.REDSTONE_BLOCK){
			        p.getWorld().playSound(p.getLocation(), Sound.ZOMBIE_REMEDY, 3.0F, 3.0F);
					p.sendMessage(prefix1 + ChatColor.RED + "Sorry, but this server isn't online at the moment!");
					p.closeInventory();
					event.setCancelled(true);
				}else if(ItemManager.LobbyItem3().getType() == Material.EMERALD_BLOCK){
					p.getWorld().playSound(p.getLocation(), Sound.LEVEL_UP, 10F, -5F);
					p.sendMessage(prefix1 + ChatColor.GRAY + "Sending to Lobby 4!");
					p.closeInventory();
					event.setCancelled(true);
				}
			}else if(event.getCurrentItem().equals(ItemManager.LobbyItem4())){
				if(ItemManager.LobbyItem4().getType() == Material.REDSTONE_BLOCK){
			        p.getWorld().playSound(p.getLocation(), Sound.ZOMBIE_REMEDY, 3.0F, 3.0F);
					p.sendMessage(prefix1 + ChatColor.RED + "Sorry, but this server isn't online at the moment!");
					p.closeInventory();
					event.setCancelled(true);
				}else if(ItemManager.LobbyItem4().getType() == Material.EMERALD_BLOCK){
					p.getWorld().playSound(p.getLocation(), Sound.LEVEL_UP, 10F, -5F);
					p.sendMessage(prefix1 + ChatColor.GRAY + "Sending to Lobby 5!");
					p.closeInventory();
					event.setCancelled(true);
				}
			}else if(event.getCurrentItem().equals(ItemManager.LobbyItem5())){
				if(ItemManager.LobbyItem5().getType() == Material.REDSTONE_BLOCK){
			        p.getWorld().playSound(p.getLocation(), Sound.ZOMBIE_REMEDY, 3.0F, 3.0F);
					p.sendMessage(prefix1 + ChatColor.RED + "Sorry, but this server isn't online at the moment!");
					p.closeInventory();
					event.setCancelled(true);
				}else if(ItemManager.LobbyItem5().getType() == Material.EMERALD_BLOCK){
					p.getWorld().playSound(p.getLocation(), Sound.LEVEL_UP, 10F, -5F);
					p.sendMessage(prefix1 + ChatColor.GRAY + "Sending to Lobby 6!");
					p.closeInventory();
					event.setCancelled(true);
				}
			}else if(event.getCurrentItem().equals(ItemManager.LobbyItem6())){
				if(ItemManager.LobbyItem6().getType() == Material.REDSTONE_BLOCK){
			        p.getWorld().playSound(p.getLocation(), Sound.ZOMBIE_REMEDY, 3.0F, 3.0F);
					p.sendMessage(prefix1 + ChatColor.RED + "Sorry, but this server isn't online at the moment!");
					p.closeInventory();
					event.setCancelled(true);
				}else if(ItemManager.LobbyItem6().getType() == Material.EMERALD_BLOCK){
					p.getWorld().playSound(p.getLocation(), Sound.LEVEL_UP, 10F, -5F);
					p.sendMessage(prefix1 + ChatColor.GRAY + "Sending to Lobby 7!");
					p.closeInventory();
					event.setCancelled(true);
				}
			}else{
				p.closeInventory();
				event.setCancelled(true);
			}
		}else if(event.getInventory().equals(DataManager.pshopinv.get(p.getName()))){
			if(event.getCurrentItem().equals(ItemManager.Mounts())){
				p.closeInventory();
				p.sendMessage(prefix1 + ChatColor.GRAY + "This feature isn't avaible yet!");
				event.setCancelled(true);
			}else if(event.getCurrentItem().equals(ItemManager.statics())){
				p.closeInventory();
				p.sendMessage(prefix1 + ChatColor.GRAY + "This feature isn't avaible yet!");
				event.setCancelled(true);
			}else if(event.getCurrentItem().equals(ItemManager.Pets())){
				p.closeInventory();
				p.sendMessage(prefix1 + ChatColor.GRAY + "This feature isn't avaible yet!");
				event.setCancelled(true);
			}else if(event.getCurrentItem().equals(ItemManager.Ingame())){
				p.closeInventory();
				p.sendMessage(prefix1 + ChatColor.GRAY + "This feature isn't avaible yet!");
				event.setCancelled(true);
			}else if(event.getCurrentItem().equals(ItemManager.Disguises())){
				p.closeInventory();
				p.sendMessage(prefix1 + ChatColor.GRAY + "This feature isn't avaible yet!");
				event.setCancelled(true);
			}else if(event.getCurrentItem().equals(ItemManager.PartiesFriends())){
				p.closeInventory();
				p.sendMessage(prefix1 + ChatColor.GRAY + "This feature isn't avaible yet!");
				event.setCancelled(true);
			}else if(event.getCurrentItem().equals(ItemManager.Particles())){
				p.closeInventory();
				event.setCancelled(true);	
			}else if(event.getCurrentItem().equals(ItemManager.gadgetsitem())){
				p.closeInventory();
				p.sendMessage(prefix1 + ChatColor.GRAY + "This feature isn't avaible yet!");
				event.setCancelled(true);	
			}else{
				p.closeInventory();
				event.setCancelled(true);
			}
		}else if(event.getInventory().equals(DataManager.pinvstatics.get(p.getName()))){
			
		}else if(event.getInventory().equals(DataManager.pgadgetinv.get(p.getName()))){
			if(event.getCurrentItem().equals(ItemManager.Fireball())){
				//IF BUYED
				DataManager.gadget.put(p.getName(), "fireball");
				p.getInventory().setItem(6, ItemManager.Wand(p));
				p.updateInventory();

				p.closeInventory();
				event.setCancelled(true);
			}else if(event.getCurrentItem().equals(ItemManager.FireWork())){
				DataManager.gadget.put(p.getName(), "firework");
				p.getInventory().setItem(6, ItemManager.Wand(p));
				p.updateInventory();
				p.closeInventory();
				event.setCancelled(true);
			}else{
				p.closeInventory();
				event.setCancelled(true);
			}
		}else if(event.getInventory().equals(DataManager.pteleporterinv.get(p.getName()))){
			if(event.getCurrentItem().equals(ItemManager.Hub())){
				BungeecordManager.sendToServer(p, "hub");
				p.closeInventory();
				event.setCancelled(true);
			}else if(event.getCurrentItem().equals(ItemManager.Games())){
				BungeecordManager.sendToServer(p, "lobbygames");
				p.closeInventory();
				event.setCancelled(true);
			}else{
				p.closeInventory();
				event.setCancelled(true);
			}
		}else if(event.getInventory().equals(DataManager.pdisguiseinv.get(p.getName()))){
			//if(event.getCurrentItem().equals(ItemManager.PigEgg())){
		//	DisguiseManager.changeDisguise(DisguiseManager.DisguiseType.SPIDER, p);
			
		}else{
			
			
		}
		
		if (event.getCurrentItem() == null
				|| event.getCurrentItem().getType() == Material.AIR
				|| !event.getCurrentItem().hasItemMeta()) {
			p.closeInventory();
			event.setCancelled(true);
		}
		
		
	}
}
