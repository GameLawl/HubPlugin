package com.system.ranksystem.listener;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.system.gadgetssystem.gadgets.GadgetManager;
import com.system.hubsystem.manager.ItemManager;
import com.system.inventorysystem.inventories.GadgetInventory;
import com.system.inventorysystem.inventories.LobbyInventory;
import com.system.inventorysystem.inventories.ShopInventory;
import com.system.inventorysystem.inventories.StaticsInventory;
import com.system.inventorysystem.inventories.TeleportInventory;

public class InteractListener implements Listener {
	public static String prefix = ChatColor.GREEN + "COMMAND>> ";
	public static String prefix1 = ChatColor.BLUE + "HUB>> ";
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		Player p = event.getPlayer();
		if (event.getAction() == Action.RIGHT_CLICK_AIR
				|| event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (p.getItemInHand().equals(ItemManager.shop())) {
				p.closeInventory();
				ShopInventory.onMethod(p, "open");
				event.setCancelled(true);
			} else if (p.getItemInHand().equals(ItemManager.teleporter())) {
				p.closeInventory();
				TeleportInventory.onMethod(p, "open");
				event.setCancelled(true);
			} else if (p.getItemInHand().equals(ItemManager.gadgets())) {
				p.closeInventory();
				GadgetInventory.onMethod(p, "open");
				event.setCancelled(true);
			} else if (p.getItemInHand().equals(ItemManager.lobby())) {
				p.closeInventory();
				LobbyInventory.onMethod(p, "open");
				event.setCancelled(true);
			} else if (p.getItemInHand().equals(ItemManager.statics())) {
				p.closeInventory();
				p.sendMessage(prefix1 + ChatColor.GRAY + "This feature isn't avaible yet!");
				//StaticsInventory.onMethod(p, "open");
				event.setCancelled(true);
				
			}else if(p.getItemInHand().equals(ItemManager.gadgetHash.get(p.getName()))){
				GadgetManager.onMethod(p, "use");
			}
		}
	}

}
