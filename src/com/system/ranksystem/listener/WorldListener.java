package com.system.ranksystem.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockIgniteEvent.IgniteCause;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import com.system.ranksystem.ParticleEffect;

public class WorldListener implements Listener{
	
	@EventHandler
	public void onWeather(WeatherChangeEvent event){
		event.setCancelled(true);
	}
	@EventHandler
	 public void noFireballFire(BlockIgniteEvent event){

		     event.setCancelled(true);

		 
	}
    @EventHandler(priority = EventPriority.LOW)
    public void onEntityExplode(EntityExplodeEvent event) {
    	event.setCancelled(true);
    }


}
