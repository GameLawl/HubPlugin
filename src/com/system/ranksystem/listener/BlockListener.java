package com.system.ranksystem.listener;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import com.system.ranksystem.manager.PermissionManager;

public class BlockListener implements Listener {
	
	@EventHandler
	public void onBreak(BlockBreakEvent event){
		Player p = event.getPlayer();
		if(PermissionManager.hasPermission(p, "buildteam") == true){
					}else{
			p.sendMessage(ChatColor.DARK_RED + "You are not allowed to break blocks!");
			event.setCancelled(true);
		}
		
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent event){
		Player p = event.getPlayer();
		if(PermissionManager.hasPermission(p, "buildteam") == true){
			
		}else{
			p.sendMessage(ChatColor.DARK_RED + "You are not allowed to place blocks!");
			event.setCancelled(true);
		}
		
	}

}
